var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

var passport = require('passport');
var router_user = require('./routes/user');
var flash = require('connect-flash');
var multer = require('multer');
var config = require('./config');
var port = process.env.PORT || config.port;
var mongoose = require("mongoose");
var sessionStore = require('mongoose-session')(mongoose);
var sessionMiddleware = session({secret: config.secret,
	             resave:false,
	             saveUninitialized: false,
	             store: sessionStore, 
	             cookie: { maxAge: 30 * 24 * 60 * 60 * 1000,
	            	       expires:new Date(Date.now() + 30 * 24 * 60 * 60 * 1000) } 
                 });
//Set up the global variables 
app.set("settings", {'root_directory':config.root_directory,
	                 'bin_directory': config.bin_directory,
	                 'title':config.title,
	                 'maxJobCount':config.maxJobCount,
	                 'process_count':0});
//Connect database
mongoose.connect('mongodb://localhost:27017/weegene');

require('./controllers/auth')(passport); // pass passport for configuration
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(sessionMiddleware);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); 
//app.use('/users-api',router_user);
var router = require("./routes/route")(app,passport);
var io = require('./socket/socket_control')(sessionMiddleware,io);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
server.listen(port, function(){
	console.log('listen to: '+port );
});
server.timeout = 10000000
module.exports = app;
