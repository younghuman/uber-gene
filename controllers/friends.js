var User = require('../models/user.js');
exports.findRandomPeople = function(req, res,next) {
	var userlist = [];
  	User.find({}, function (err, user) {
  		 if(err) next(err);
  		 for (var i=0;i<user.length;i++){
  			 if(user[i].username!=req.user.username)
  				 {
  			 userlist.push({'email':user[i].username, 
  				              'name':user[i].data.name,
  				              'id':user[i]._id}); 
  				 }
  			}
         if(userlist.length>10)  {
  			 userlist = shuffle(userlist);
  			 userlist = userlist.slice(0,10);
  		 }
         req.userlist = userlist;
         next();
  	});
}

function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};