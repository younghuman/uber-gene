var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
module.exports = function(passport)
{
passport.serializeUser(function(user, done) {
    done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});


passport.use('local-signin', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
},
function(req, email, password, done) { // callback with email and password from our form
	if(req.body.remember) {
		 //console.log("remember");
	     var month = 30 * 24 * 60 * 60 * 1000
    	 req.session.cookie.maxAge = month;
	     req.session.cookie.expires= new Date(Date.now() + month);
    	 
     }
     else{
    	 req.session.cookie.expires = false;
    	 req.session.cookie.maxAge = 60000;
     } 
	
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOne({ 'username' :  email }, function(err, user) {
        // if there are any errors, return the error before anything else
    	 
    	if (err)
        	{
            return done(err);
        	}
        // if no user is found, return the message
        if (!user){
         return done(null, false, req.flash('signinMessage', 'Email not found.')); // req.flash is the way to set flashdata using connect-flash
        }
        // if the user is found but the password is wrong
        user.verifyPassword(password, function(err, isMatch)
        		{
            if(err) return done(err); 
        	if(!isMatch) return done(null, false, req.flash('signinMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
        	return done(null, user);	
        		});
        // all is well, return successful user
        
    });

}));
};