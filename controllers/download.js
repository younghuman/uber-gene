var printVariants = function (object, string, disease_column){
	if(disease_column==null) disease_column = "disease";
	for (gene in object){
		string += gene+">>\n";
		for (variant in object[gene]){
			string += object[gene][variant][disease_column]+"\t"+object[gene][variant].everything+"\n";
		}
	}
	return string;
}
exports.PersonalReport = function (req, res, next){
	clinical =req.user.data.clinical;

	res.format({
		'text/plain': function(){
			var string = "";
			string +="GWAS report for odds of qualitative result>>>\n";
			string +="Disease\tPop Risk\tConfidence\tSnp\n";
            var odds = clinical.gwas.odds
			for (i in odds){
				var line = [odds[i].disease,odds[i].pop_risk,odds[i].conf,odds[i].snp].join("\t") + "\n";
				string += line;
			}
			string +="Disease\tPop Difference\tConfidence\tSnp\n";
			var beta = clinical.gwas.beta;
			for (i in beta){
				var line = [beta[i].disease,beta[i].pop_risk,beta[i].conf,beta[i].snp].join("\t") + "\n";
				string += line;
			}
			string+="Clinvar rare variants>>>\n";
			var clinvar = clinical.clinvar;
		    string = printVariants(clinvar, string);
		    var ambry = clinical.ambry;
			string+="Ambry rare variants>>>\n"; 
			string = printVariants(ambry, string);
		    string+="ACMG rare variants>>>\n";
		    var acmg = clinical.acmg;
		    string = printVariants(acmg, string);
		    var phenolyzer = clinical.phenolyzer;
		    string+="OMIM,Orphanet,GeneReviews rare variants>>>\n"; 
			string = printVariants(phenolyzer, string,"gene_disease");
			var carrier = clinical.carrier;
			string+="Rare variants for carrier status screen>>>\n"; 
			string = printVariants(carrier, string);
			var svm = clinical.scm;
			string+="Predicted deleterious rare variants>>>\n"; 
			string = printVariants(svm, string);
			res.send(string);
		}
	},
	 {
	    'application/json':function(){
	    	res.send(data);
	    }	
	 } 
	)
	
}