var User = require('../models/user');
module.exports = function(req,res,next){
	User.findOne({ 'username': req.body.email }, function (err, user) {
		if(err) next(err);
		if(user) {
			req.flash("signupMessage","The email has already been taken!" );
			res.redirect('/'); 
		}
		else{
			 var newUser        = new User();
			 newUser.username   = req.body.email;
        	 newUser.password = newUser.generateHash(req.body.password); 
        	 newUser.data = {'name':req.body.name}
        	 newUser.save(function(err) {
                    if (err)
                        next(err);
                    req.login(newUser, function(err){
                    	if(err) next(err);
                    	req.flash("signupMessage","Congratulations!" );
                    	res.redirect('/profile');
                    });
                });
		}
	});
}
	
