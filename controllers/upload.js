var User = require('../models/user');
var fs = require('fs');
var multer = require('multer');
var child_process= require('child_process');
var path = require('path');
var sleep = require('sleep');
//File upload parameter setup
exports.multer = function(req, res, next) {
		   fs.mkdir('../upload/'+ req.user._id,function(err){
		   var handler = multer({
			    dest: './upload/'+req.user._id,  
			    rename: function(fieldname, filename) {
			    	 return makeid()+Date.now();
			    },
			    onFileUploadStart: function (file) {
			      // You now have access to req
			      console.log(file.fieldname + ' is uploading...')
			    },
			    onFileUploadComplete: function (file) {
			      console.log(file.fieldname + ' uploaded to  ' + file.path);
			      done=true;
			    }
			  });
		   return handler(req, res, next);
		   });
};
exports.process = function(req, res, next) {
	    var filetype = req.body.filetype;
	    var if_genome= req.body.coverage;
	    var bin_dir = req.app.get('settings').bin_directory;
	    var root_dir = req.app.get('settings').root_directory;
	    var TITLE = req.app.get('settings').title;
	    var work_dir = root_dir+"/"+path.dirname(req.files.file.path);
	    var cmd = "perl "+bin_dir+"/pipeline/variant2report.pl "
	                +root_dir+"/"+req.files.file.path+" -json -out "+
	                work_dir;
	    if(filetype=="23andme") cmd+=' -format 23andme';
	    var cmd_check = cmd+' -check';
	    var date = new Date();
            date = date.toLocaleTimeString() +" "+ date.toLocaleDateString();
        var reportReady = false;
        if(req.user.data.bools && req.user.data.bools.reportReady==true)
        	reportReady = true;
	    //Check if the input file is legal
	    var error_msg = null;
      try { 
	    var result = child_process.execSync(cmd_check);
	  }
	  catch (error)
	  {
	    	error_msg = error.stderr.toString().split("\n")[0];
            console.log(error_msg);
	    	child_process.exec("rm -f "+work_dir+"/*");
 		     res.render("profile", 
 		      { user: req.user,
              title: TITLE,
              page:'me',
              subpage:'upload',
              error: error_msg });

      }
	if(!error_msg)
	{
	     var systemLogs = {};
	     var id = 1;
		 if(req.user.data && req.user.data.systemLogs)
	     { 
	        systemLogs = req.user.data.systemLogs;
	        id = Math.max(Object.keys(systemLogs).map(parseInt))+1;
	     } 
	     var log = {
	    		     "message":"The server is trying hard to generate your report!", 
	    			 "date":date 
	    			 }
	     systemLogs[id.toString()] = log; 
	     //This code checks the current server load, if there are too many jobs then just sleep
	     while(1){
	    	 var settings = req.app.get('settings');
             if(settings.process_count < settings.maxJobCount){
            	 break;
             }
             sleep.sleep(10);
	     }
	     var settings = req.app.get('settings');
             settings.process_count++;
             req.app.set('settings',settings);
	   //Update system logs before executing the background program
	     req.user.update({"data.systemLogs":systemLogs,
	    	              "data.bools.reportReady":false,
	    	              "data.clinical": null,
	    	              "data.bools.running":true },null,
	          function(){
	          child_process.exec(cmd, {maxBuffer:Infinity}, function(error,stderr,stdout){
               //Server process count minus one
	    		   var settings = req.app.get('settings');
                       settings.process_count--;
                   req.app.set('settings',settings);
	        	if(error){
	    		console.log(error);
	    		var date = new Date();
                date = date.toLocaleTimeString() +" "+ date.toLocaleDateString();
	    		var log = {
		    		 "message":"We are unable to generate your report, sorry...", 
		    	     "date":date 
		        }
	    		systemLogs[id.toString()] = log;
	    		req.user.update({"data.systemLogs":systemLogs}); 
	    		return;
	    	  }
	    	  if(stderr){
	    		fs.appendFile(work_dir+"/run_log",stderr);
	    	  }
	    	  if(stdout){
	    		fs.appendFile(work_dir+"/run_log",stdout);
	    	  }
	    	  fs.readFile(work_dir+"/report.json", function(err, data){
	    		if(err) throw err;
    			var date = new Date();
                date = date.toLocaleTimeString() +" "+ date.toLocaleDateString();
    			var log = {
    				 "message":"Your report is ready to see!",
    				 "date":date
    			}
    			systemLogs[id.toString()] = log;
	    		clinical_data = JSON.parse(data);
	    		req.user.update({"data.clinical":clinical_data,
	    			             "data.bools.reportReady":true,
	    			             "data.systemLogs":systemLogs,
	    			             "data.bools.running":false },null,function(err){
	    			if(err) console.log(err);
	    			child_process.exec("rm -rf "+work_dir+"/*");
            	});
	    	})
        });
	    res.redirect("/profile");
	   });
	} // if(!error_msg)
     
};

function makeid(len)
{
	len = typeof len !== 'undefined' ? len : 5;
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < len; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}