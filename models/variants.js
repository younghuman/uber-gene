var mongoose = require('mongoose');
var variantSchema = new mongoose.Schema({
    username: {
    	type:String,
    	unique: true,
    	required:true
    },
    data:  mongoose.Schema.Types.Mixed,    
});
module.exports = mongoose.model('Variants', variantSchema);
