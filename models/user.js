var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = new mongoose.Schema({
    username: {
    	type:String,
    	unique: true,
    	required:true
    },
    password: {
    	type: String,
    	required:true  
    },
    data:  mongoose.Schema.Types.Mixed,    
});
	userSchema.methods.generateHash = function(password)
	{
		return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
	};
	userSchema.methods.verifyPassword = function(password, cb){
		bcrypt.compare(password, this.password, function(err, isMatch){
			if(err) return cb(err);
			cb(null, isMatch); 
		});
		
	};
	module.exports = mongoose.model('User', userSchema);
