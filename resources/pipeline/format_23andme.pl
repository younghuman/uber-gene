use strict;
use warnings;
my $buildver = "hg19";
my %change_hash = ("36"=>"hg18","37"=>"hg19");
my $illegal = 0;
@ARGV == 2 or die;
my $inputfile = $ARGV[0];
my $outfile = $ARGV[1];
my $fh;
open(INPUT, $inputfile) or die;
while(<INPUT>){
    s/[\r\n]+//g;	
	my $line = $_;
	if($line =~ /^#/){
		if($line =~ /assembly build (\d+)/i)
		{
	     $buildver = $change_hash{$1};
		}
	}
	else{
		open($fh, ">${outfile}_$buildver") or die if(not $fh);
	    if($line!~/^\w+\t\w{1,4}\t\d+\t\S{1,2}$/){
	       print STDERR "ERROR: Your file is not 23andme file. You have an illegal line here: $line\n" and die;
	    }
        my ($id, $chr, $pos, $genotype) = split("\t", $line);	
        $chr = "M" if($chr eq "MT");    
	    print $fh join("\t", ($chr,$pos,$pos))."\n";
	}
}
print STDOUT $buildver;

