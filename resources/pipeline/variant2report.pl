use warnings;
use strict;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use JSON;
my $dirname = dirname(__FILE__);
our($out_dir, $if_json, $if_genome, $maf, $format, $check_file,
    $version);
GetOptions('out=s'=>\$out_dir, 'json'=>\$if_json, 
           'genome'=>\$if_genome, 'format=s'=>\$format,
           'maf=s'=>\$maf, 'check'=>\$check_file,
           'version=s'=>\$version );
$maf ||= 0.02;
$out_dir||=".";
$format ||="vcf";
$version ||= 4;
@ARGV or pod2usage (-verbose=>0, -exitval=>1, -output=>\*STDOUT);
@ARGV==1 or die;
my $input_file = $ARGV[0];
my $dir = "$dirname/..";
if ($format eq "23andme") {
	process23andme();
}
elsif($format eq "vcf"){
    checkVCF();
}
if(not $check_file){
    runAnnotation();
    generateReport();
}
    
sub process23andme {
  my $cmd = "perl $dir/pipeline/format_23andme.pl $input_file $out_dir/23andme";
  my $buildver = `$cmd` or (print STDERR "ERROR: Your 23andme file is illegal!\n" and die);
  
  if(not $check_file){
	$cmd = "perl $dir/annovar/retrieve_seq_from_fasta.pl -format tab "
	      ."-seqfile $dir/annovar/humandb/seq/${buildver}_chrall.fa $out_dir/23andme_$buildver "
	      ."-outfile $out_dir/23andme_seq";
	system($cmd) and die "ERROR: The reference alleles cannot be retrieved!\n";
	open(SEQ,"$out_dir/23andme_seq") or die;
	my %ref_hash = ();
	my ($pos,$allele);
	for (<SEQ>){
		s/[\r\n]+//g;
		next if not $_;
		if(/^>(\w+?:\d+?-\d+?)\s/){
			$pos = $1;
		}
		elsif(/^(\w)$/){
			$allele = $1;
			$ref_hash{$pos} = $allele;
		}
	}
	close(SEQ);
	open(INPUT, $input_file) or die;
	open(OUTPUT, ">$out_dir/input.vcf") or die;
	print OUTPUT "##fileformat=VCFv4.2\n";
	print OUTPUT "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tGENOTYPE\tZYGOSITY\n";
	my ($num, $hom_num, $het_num)= (0,0,0);
	for(<INPUT>){
		next if(/^#/);
		s/[\r\n]+//g;
		my ($id, $chr, $pos, $genotype) = split("\t");
		$num++;
		if($genotype=~/[^ATCGatcg]/){
			print STDERR "WARNING: $id does not have a signal, thus neglected!\n";
			next;
		}
		$chr = "M" if $chr eq "MT";
		$genotype = uc $genotype;
		my @alleles = split("",$genotype);
		my $pos_l = "$chr:$pos-$pos";
        my $ref = $ref_hash{$pos_l};
        my ($alt,$gt,$zyg) = ("","","");
        if(@alleles == 1){
        	if($alleles[0] eq $ref){
        		#print STDERR "WARNING: $id has the same allele as reference genome, thus neglected!\n";
        		next;
        	}
        	else{
        		$alt = $alleles[0];
        		$zyg = "hom";
        		$gt = "1";
        		$hom_num++;
        	}
        }
        elsif(@alleles==2){
        	if($alleles[0] eq $ref and $alleles[1] eq $ref){
        		#print STDERR "WARNING: $id has the same alleles as reference genome, thus neglected!\n";
        		next;
        	}
        	elsif($alleles[0] eq $ref and $alleles[1] ne $ref){
        		$alt = $alleles[1];
        		$zyg = "het";
        		$gt = "0/1";
        		$het_num++;
        	}
        	elsif($alleles[0] ne $ref and $alleles[1] eq $ref){
        		$alt = $alleles[0];
        		$zyg = "het";
        		$gt = "0/1";
        		$het_num++;
        	}
        	elsif($alleles[0] ne $ref and $alleles[1] ne $ref){
        		if($alleles[0] eq $alleles[1]){
        			$alt = $alleles[0];
        			$zyg = "hom";
        			$gt = "1/1";
        			$hom_num++;
        		}
        		else{
        			$alt = "$alleles[0],$alleles[1]";
        			$zyg = "het";
        			$gt = "1/2";
        			$het_num++;
        		}
        	}
        }
		my $output = join("\t",($chr,$pos,$id,$ref,$alt,".",".",".","GT",$gt,$zyg))."\n";
		print OUTPUT $output;
     } #Finish processing input file
     $input_file = "$out_dir/input.vcf";
     print STDERR "NOTICE: In total $num lines processed, including $hom_num homozygous variants and $het_num heterozygous variants! \n";
  }# if not $check_file
}

sub checkVCF {
	system ("perl $dir/annovar/convert2annovar.pl -format vcf4 $input_file >/dev/null") and 
	print STDERR "ERROR:Your input file is not a legal VCF file!\n" and die;
}

sub runAnnotation {
	system("perl $dir/annovar/table_annovar.pl $input_file $dir/annovar/humandb/ -vcfinput -buildver hg19 -protocol refGene,custom_wannovar20150205 "
	      ."-operation g,f -outfile $out_dir/final >$out_dir/annovar_log 2>$out_dir/annovar_log") and 
	print STDERR "ERROR:The table_annovar.pl step fails, please check annovar_log!\n" and
	die;
}
sub generateReport {
	my $cmd = "perl $dir/clinical_report/clinical_report.pl $out_dir/final.hg19_multianno.txt -maf $maf -out $out_dir/report";
	$cmd .=  ".json -json "   if($if_json);
	$cmd .=  " -genome " if($if_genome);
	$cmd .=  " 2>$out_dir/clinical_log";
	system($cmd) and print STDERR "ERROR:The clinical_report.pl step fails, please check clinical_log\n" and 
	die;
}

=head1 SYNOPSIS

 perl variant2report.pl <annotated_file>  [arguments]
     arguments:
           -out           The directory to save the results. (default:current dir) 
           -json          Output json instead of text
           -genome        If the input file covers the whole genome
           -format        23andme|vcf  (default:vcf)
           -maf           The minor allele frequency for filtering 
           -check         Only check if the file is legal without processing
=cut


