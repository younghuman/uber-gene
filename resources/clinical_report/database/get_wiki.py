import wikipedia as wk
import re
import json
def main():
    file1 = open("diseases","r")
    output = {}
    disease_wiki = {}
    for disease in file1:
        disease = re.sub(r'[\r\n]+','',disease)
        suggestions = wk.search(disease)
        output[disease] = []
        for i in range(len(suggestions)):
            if i>1: break
            try:
                summary = wk.summary(suggestions[i],sentences=1)
            except:
                continue;
            output[disease].append({"name":suggestions[i],
                                    "description":summary})
            if suggestions[i] not in disease_wiki:
                page = wk.page(suggestions[i])
                disease_wiki[suggestions[i]]={
                   "content":page.content,
                   "images":page.images   }
    file2 = open("disease_summary","w")
    file3 = open("wiki_content","w")
    file2.write(json.dumps(output,indent=3))
    file3.write(json.dumps(disease_wiki,indent=3))

if __name__ == "__main__":
    main();
    
    