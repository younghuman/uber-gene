#!/usr/bin/env perl
use warnings;
use strict;
use Getopt::Long;
use Pod::Usage;

our $REVISION = '$Revision: d0d72aa1e51ca9f3dc6b0611a4972956d6e93dec $';
our $DATE =	'$Date: 2014-07-14 03:13:01 -0700 (Mon, 14 Jul 2014) $';  
our $AUTHOR =	'$Author: Kai Wang <kai@openbioinformatics.org> $';

our ($verbose, $help, $man);



GetOptions('verbose|v'=>\$verbose, 'help|h'=>\$help, 'man|m'=>\$man, ) or pod2usage ();

$help and pod2usage (-verbose=>1, -exitval=>1, -output=>\*STDOUT);
$man and pod2usage (-verbose=>2, -exitval=>1, -output=>\*STDOUT);
@ARGV or pod2usage (-verbose=>0, -exitval=>1, -output=>\*STDOUT);
@ARGV == 1 or pod2usage ("Syntax error");

my ($multiannofile) = @ARGV;

				
predictAncestry ($multiannofile);


sub predictAncestry {
	my ($multiannofile) = @_;
	open (FH, $multiannofile) or die "Error: cannot read from input multianno file $multiannofile: $!\n";
	$_ = <FH>;
	s/[\r\n]+$//;
	my @field = split (/\t/, $_);
	my ($index_afr, $index_amr, $index_eas, $index_eur, $index_sas, $index_zygosity);
	
	for my $i (0 .. @field-1) {
		if ($field[$i] eq '1000G_AFR') {
			$index_afr = $i;
		} elsif ($field[$i] eq '1000G_AMR') {
			$index_amr = $i;
		} elsif ($field[$i] eq '1000G_EAS' or $field[$i] eq '1000G_ASN') {
			$index_eas = $i;
		} elsif ($field[$i] eq '1000G_EUR') {
			$index_eur = $i;
		} elsif ($field[$i] eq '1000G_SAS') {
			$index_sas = $i;
		} elsif ($field[$i] eq 'Zygosity') {
			$index_zygosity = $i;
		}
	}
	$verbose and print STDERR "NOTICE: the five indeces are $index_afr, $index_amr, $index_eas, $index_eur, $index_sas\n";
	
	my (@loglh, @ethnicity);		#log likelihood for this ethnicity group
	while (<FH>) {
		s/[\r\n]+$//;
		@field = split (/\t/, $_);
		#print STDERR "NOTICE: total ", scalar (@field), " fields are found in line: @field\n";
		my @freq = @field[$index_afr, $index_amr, $index_eas, $index_eur, $index_sas];
		my $zyg = '';
		for (my $i=0;$i<@field;$i++){
			if(/^(hom|het)$/i) {  $zyg = $1; }
		    elsif(/AC=(\S+?)(;|$)/) {
		    	my $ac = $1;
		    	if($ac==2) { $zyg= 'hom' ;}
		    	else {$zyg='het'; }
		    	
		    }
		}
		defined $index_zygosity and $zyg = $field[$index_zygosity];
		
		for my $i (0 .. 4) {
			
			defined $freq[$i] or next;
			
			$freq[$i] eq '.' and $freq[$i] = 0.001;
			$freq[$i] = 0.001 if not $freq[$i];
			if ($zyg eq 'hom') {
				$loglh[$i] += log ($freq[$i]*$freq[$i]);
			} elsif ($zyg eq 'het') {
				$loglh[$i] += log (2*$freq[$i]*(1-$freq[$i]));
			} else {
				$loglh[$i] += log ($freq[$i]);
			}
		}
	}
	
	for my $i (0 .. 4) {
		$loglh[$i] ||= -1e-9;
	}
	
	print STDERR "NOTICE: The log likelihood for five populations (AFR, AMR, EAS, EUR, SAS) are @loglh\n";
	
	push @ethnicity, [$loglh[0], 'African'], [$loglh[1], 'Admixed African/European'], [$loglh[2], 'East Asian'], [$loglh[3], 'European'], [$loglh[4], 'South Asian'];
	@ethnicity = sort {$b->[0]<=>$a->[0]} @ethnicity;
	
	print STDERR "NOTICE: The sample is predicted as $ethnicity[0]->[1]\n";
	
}


=head1 SYNOPSIS

 convert2annovar.pl [arguments] <variantfile>

 Optional arguments:
        -h, --help                      print help message
        -m, --man                       print complete documentation
        -v, --verbose                   use verbose output

 Function: convert variant call file generated from various software programs 
 into ANNOVAR input format
 
 Example: 
 Version: $Date: 2014-07-14 03:13:01 -0700 (Mon, 14 Jul 2014) $

=head1 OPTIONS

=over 8

=item B<--help>

print a brief usage message and detailed explanation of options.

=item B<--man>

print the complete manual of the program.

=item B<--verbose>

use verbose output.

=back

=head1 DESCRIPTION

This program is used to predict ancestry origin of a given sequenced sample in AVINPUT format.


For questions or comments, please contact kai@openbioinformatics.org.

=cut
