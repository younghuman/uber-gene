#!/usr/bin/env perl
use warnings;
use strict;
use Pod::Usage;
use Getopt::Long;
use File::Basename;
use JSON;
our $REVISION = '$Revision: c87005ee794b2ac518aaebeb608132ca3754e1f6 $';
our $DATE =	'$Date: 2013-11-12 18:10:49 -0800 (Tue, 12 Nov 2013) $';  
our $AUTHOR =	'$Author: Kai Wang <kai@openbioinformatics.org> $';

our ($verbose, $help, $man);
our ($avgwasfile, $sumfile);
our ($freqfile, $freqcol, $json, $if_genome);

GetOptions('verbose|v'=>\$verbose, 'help|h'=>\$help, 'man|m'=>\$man, 'freqfile=s'=>\$freqfile,
           'freqcol=s'=>\$freqcol, 'json'=>\$json, 'genome'=>\$if_genome ) or pod2usage ();
	
$help and pod2usage (-verbose=>1, -exitval=>1, -output=>\*STDOUT);
$man and pod2usage (-verbose=>2, -exitval=>1, -output=>\*STDOUT);
@ARGV or pod2usage (-verbose=>0, -exitval=>1, -output=>\*STDOUT);
@ARGV == 2 or pod2usage ("Syntax error");
$freqcol||= 'all';
my $freqname = "1000g2014oct_".$freqcol;
($avgwasfile, $sumfile) = @ARGV;

defined $freqfile or pod2usage ("Error in argument: you must specify the --freqfile argument");

my (%all_binary, %all_quant, %binary_conf, %quant_conf, %output);

my $popfreq = readFreq ($freqfile);
my $avgwas = readAVGWAS ($avgwasfile, $popfreq);

scanGenomeSummary ($sumfile, $popfreq);



sub readFreq {
	my ($freqfile) = @_;
	my ($freq);
	open (FREQ, $freqfile) or die "Error: cannot read from freqfile $freqfile: $!\n";
	$_ = <FREQ>;
	my @header = split (/\t/, $_);
	my ($ifreq);
	for my $i (0 .. @header-1) {
		if ($header[$i] =~ m/^$freqname/i) {
			$ifreq = $i;
			last;
		}
	}
	defined $ifreq or die "Error: ifreq are not found in the freqfile $freqfile\n";
	
	while (<FREQ>) {
		s/[\r\n]+$//;
		my @field = split (/\t/, $_);
		$field[0] =~ s/^chr//;
		
		@field > 5 or die "Error: at least five tab-delimited columns are required in freqfile $freqfile: <$_>\n";
		defined $field[$ifreq] or die "Error: unable to find freq information from line: <$_>\n";
		my $snp_info = join ("\t", @field[0..4]);
		$freq->{$snp_info} = $field[$ifreq];
	}
	return ($freq);
}



sub scanGenomeSummary {
	my ($sumfile, $popfreq) = @_;
	open (SUM, $sumfile) or die "Error: cannot read from sumfile $sumfile: $!\n";
	$_ = <SUM>;
	my @header = split (/\t/, $_);
	my ($izyo, $ifreq);
	for my $i (0 .. @header-1) {
		if ($header[$i] =~ m/^Otherinfo/) {
			$izyo = $i;
			last;
		}
	}
	for my $i (0 .. @header-1) {
		if ($header[$i] =~ m/^1000g(.*?)_$freqcol/i) {
			$ifreq = $i;
			last;
		}
	}
	defined $ifreq or die "Error: izyo and ifreq are not found in the genome summary file\n";
	
	my (%dis_or, %dis_beta, %dis_pubmed, %dis_snp, %dis_snp_orbeta, %dis_popor, %dis_popbeta);		#four hashes, key is disease
	my ($prechr, $prestart, $predisease);
	while (<SUM>) {
		s/[\r\n]+//g;
		next if not $_;
		my @field = split (/\t/, $_);
		my ($chr, $start, $end, $ref, $alt) = @field[0..4];
		$chr =~ s/^chr//;
		

		
		my $snp_info = "$chr\t$start\t$end\t$ref\t$alt";
		my $zyg = $field[$izyo];
		for (@field){
			if( /^(hom|het)$/) { $zyg =$_; }
		}
		my $extra;
		#In case the het and hom are not directly displayed
		if($zyg ne 'het' and $zyg ne 'hom')
		{
		
		for (@field){
			  if( /^\w+=\S+;\w+=\S+;/) {$extra = $_;   } 
		   }
		if(not $extra) { 
         	die "Error: The vcf tags are needed!!\n"; }
		my @vcf_tags = split(';', $extra);
        my %vcf_tags = ();
        for my $each(@vcf_tags){
                    	my ($name, $content) = split('=', $each);
                    	$vcf_tags{$name} = $content;
                    }
        if(not defined $vcf_tags{'AC'}) { die "Error: The vcf tag AC is needed!!\n"; }
        if($vcf_tags{'AC'}=~/^1/) {  $zyg='het';  }
        else { $zyg = 'hom'; }            
		}       
		my $freq;
		
		#$freq = $field[$ifreq];		#we used to read this information from the sum file
		#$freq eq '.' and $freq = 0.001;		#rare alleles are treated as 0.1% frequency
		
		$zyg eq 'het' or $zyg eq 'hom' or die "Error: zygosity not found $zyg\n";
		if ($avgwas->{$snp_info}) {

			$freq = $popfreq->{$snp_info} || 0.001;
			$freq eq '.' and $freq = 0.001;		#we now use freqinformation from the -freqfile for calculation

			my @info = split (/\t/, $avgwas->{$snp_info});
			@info >= 2 and warn "one snp with two diseases\n";
			for my $i (0 .. @info-1) {
				my $info = $info[$i];
				my ($disease, $or, $beta, $pubmed, $strongsnp) = split ("###", $info);
				my $or_beta = 0;
				if ($prechr) {
					if ($prechr eq $chr ) {
						$start >= $prestart-5 or die "Error: the input file is not sorted by Chr and Start: chr=$chr prestart=$prestart start=$start\n";	#with 5 base from each other (due to indel renumbering coordinate)
						#$start-$prestart < 100_000 and print STDERR "WARNING: Skipping $chr:$start ($_) due to being close to $chr:$prestart\n" and next;		#only check those 100kb away
					}
				}				
				
				if ($or ne '.') {
					$beta ne '.' and die "Error: both OR and beta exist at $info!\n";
					if ($zyg eq 'het') {
						$dis_or{$disease} ||= 1;
						$dis_popor{$disease} ||= 1;
						$dis_or{$disease} *= $or;
						$dis_snp_orbeta{$disease} .= ",$or";
						$or_beta = $or;
						$dis_popor{$disease} *= $or / ((1-$freq)*(1-$freq)+2*$freq*(1-$freq)*$or+$freq*$freq*$or*$or);
					} elsif ($zyg eq 'hom') {
						$dis_or{$disease} ||= 1;
						$dis_popor{$disease} ||= 1;
						$dis_or{$disease} *= ($or*$or);
						$dis_snp_orbeta{$disease} .= ",".($or*$or);
						$or_beta = $or*$or;
						$dis_popor{$disease} *= $or*$or / ((1-$freq)*(1-$freq)+2*$freq*(1-$freq)*$or+$freq*$freq*$or*$or);
					}
				}
				if ($beta ne '.') {
					$or ne '.' and die "Error: both OR and beta exist at $info!\n";
					if ($zyg eq 'het') {
						$dis_beta{$disease} ||= 0;
						$dis_popbeta{$disease} ||= 0;
						$dis_beta{$disease} += $beta;
						$dis_snp_orbeta{$disease} .= ",$beta";
						$or_beta = $beta;
						$dis_popbeta{$disease} += ($beta - ((1-$freq)*(1-$freq)+2*$freq*(1-$freq)*$beta+$freq*$freq*2*$beta));
					} elsif ($zyg eq 'hom') {
						$dis_beta{$disease} ||= 0;
						$dis_popbeta{$disease} ||= 0;
						$dis_beta{$disease} += (2*$beta);
						$dis_snp_orbeta{$disease} .= ",".(2*$beta);
						$or_beta = 2*$beta;
						$dis_popbeta{$disease} += (2*$beta - ((1-$freq)*(1-$freq)+2*$freq*(1-$freq)*$beta+$freq*$freq*2*$beta));
					}
				}
			if(not $dis_pubmed{$disease}) { $dis_pubmed{$disease} = $pubmed;}
			else          {  $dis_pubmed{$disease} .= ",".$pubmed;  } 
			if(not $dis_snp{$disease}) { $dis_snp{$disease} = $strongsnp."($or_beta,$zyg,$freq)";}
			else          {  $dis_snp{$disease} .= ",".$strongsnp."($or_beta,$zyg,$freq)";  } 	
			}
			($prechr, $prestart) = ($chr, $start);
			delete $avgwas->{$snp_info};
		} #$avgas->{$snpinfo}
	}
	
	for my $snp_info (keys %$avgwas) {
		my @info = split (/\t/, $avgwas->{$snp_info});
		for my $i (0 .. @info-1) {
			my $info = $info[$i];
			my ($disease, $or, $beta, $pubmed, $strongsnp) = split ("###", $info);
			#print STDERR $strongsnp."\n";
			my $or_beta = 0;
			my $freq = $popfreq->{$snp_info} || 0.001;
			$freq eq '.' and $freq = 0.001;		#we now use freqinformation from the -freqfile for calculation
		
			$dis_popor{$disease} ||= 1;
			$dis_popbeta{$disease} ||= 0;
			if ($or ne '.') {
				$dis_snp_orbeta{$disease} .= ",1";
				$or_beta = 1;
				if($if_genome) { $dis_popor{$disease} *= 1 / ((1-$freq)*(1-$freq)+2*$freq*(1-$freq)*$or+$freq*$freq*$or*$or);  }
				
			} else {
				$dis_snp_orbeta{$disease} .= ",0";
				$or_beta = 0;
				if($if_genome)  { $dis_popbeta{$disease} += (0 - ((1-$freq)*(1-$freq)+2*$freq*(1-$freq)*$beta+$freq*$freq*2*$beta)); }
			}
			if(not $dis_pubmed{$disease}) { $dis_pubmed{$disease} = $pubmed;}
			else          {  $dis_pubmed{$disease} .= ",".$pubmed;  } 
			if(not $dis_snp{$disease}) { $dis_snp{$disease} = $strongsnp."($or_beta,na,$freq)";}
			else          {  $dis_snp{$disease} .= ",".$strongsnp."($or_beta,na,$freq)";  } 	
		}
	}
	
my (@odds_out,@beta_out);	
    if(not $json)
    {	
	print STDOUT "---------------------------------Complex Diseases (disease, reference, relative risk, pubmed, SNP(SNP OR), conf)-------------------------------------\n";
	print STDOUT join ("\t", qw/complex_disease risk risk_population pubmed SNP(SNP_OR) conf/), "\n";
    }
	for my $key (sort { if(not defined $dis_popor{$a})
		                   { return 1;  }
		                if(not defined $dis_popor{$b})
		                   { return -1; }
			          return $dis_popor{$b} <=> $dis_popor{$a} } keys %all_binary) {
		my $conf = $binary_conf{$key};
		
		if ( $dis_or{$key}) {
			if(not $json) { print STDOUT join ("\t", $key, $dis_or{$key}, $dis_popor{$key}, $dis_pubmed{$key}, $dis_snp{$key}, $conf), "\n"; }
			else {  
				     my %out = ();
			        	$out{'disease'}= $key;
			        	$out{'ref_risk'}=$dis_or{$key};
			        	$out{'pop_risk'}=$dis_popor{$key};
			        	$out{'pubmed'}  =$dis_pubmed{$key};
			        	$out{'snp'}     =$dis_snp{$key};
			        	$out{'conf'}    =$conf;    
				        push @odds_out,\%out; }
		} else {
			if(not $json) { print STDOUT join ("\t", $key, 1, $dis_popor{$key}, $dis_pubmed{$key}, $dis_snp{$key}, $conf), "\n"; }
			else {   
                    my %out = ();
			        	$out{'disease'}= $key;
			        	$out{'ref_risk'}=1;
			        	$out{'pop_risk'}=$dis_popor{$key};
			        	$out{'pubmed'}  =$dis_pubmed{$key};
			        	$out{'snp'}     =$dis_snp{$key};
			        	$out{'conf'}    =$conf;    
				        push @odds_out,\%out;
			        }
		}
		
	 }
    
    if(not $json)
    {	
	print STDOUT "\n------------------------------Quantitative Traits (disease, reference, relative diff, pubmed, SNP, SNP beta, conf)----------------------------------------\n";
	print STDOUT join ("\t", qw/trait diff diff_population pubmed SNP SNP_beta conf/), "\n";
    }
	for my $key ( sort { if(not defined $dis_popbeta{$a})
		                   { return 1;  }
		                if(not defined $dis_popbeta{$b})
		                   { return -1; } 
			       return $dis_popbeta{$b} <=> $dis_popbeta{$a}; } keys %all_quant) {
		my $conf = $quant_conf{$key};
		if ( $dis_beta{$key}) {
			if(not $json) { print STDOUT join ("\t", $key, $dis_beta{$key}, $dis_popbeta{$key}, $dis_pubmed{$key}, $dis_snp{$key},  $conf), "\n"; }
			        else  { 
			        	my %out = ();
			        	$out{'disease'}= $key;
			        	$out{'ref_risk'}=$dis_beta{$key};
			        	$out{'pop_risk'}=$dis_popbeta{$key};
			        	$out{'pubmed'}  =$dis_pubmed{$key};
			        	$out{'snp'}     =$dis_snp{$key};
			        	$out{'conf'}    =$conf;   
			        	push @beta_out,\%out     }
		} else {
			if(not $json) { print STDOUT join ("\t", $key, 0, $dis_popbeta{$key}, $dis_pubmed{$key}, $dis_snp{$key}, $conf), "\n"; }
			        else  { 
			        	my %out = ();
			        	$out{'disease'}= $key;
			        	$out{'ref_risk'}=0;
			        	$out{'pop_risk'}=$dis_popbeta{$key};
			        	$out{'pubmed'}  =$dis_pubmed{$key};
			        	$out{'snp'}     =$dis_snp{$key};
			        	$out{'conf'}    =$conf;   
			        	push @beta_out,\%out;  }
		}
	}
	if($json){
		$output{'odds'} = \@odds_out;
	    $output{'beta'} = \@beta_out;
	    print STDOUT to_json(\%output);
	}
}



sub readAVGWAS {
	my ($avgwasfile) = @_;
	my $avgwas;
	my $badp=0;
	
	open (AVGWAS, $avgwasfile) or die "Error: cannot read from avgwasfile $avgwasfile: $!\n";
	while (<AVGWAS>) {
		s/[\r\n]+$//;
		my ($chr, $start, $end, $ref, $alt, $info) = split (/\t/, $_);
		$chr =~ s/^chr//;
		
		my $snpinfo =  $chr."\t".$start."\t".$end."\t".$ref."\t".$alt;
		
		$info =~ m/^DISEASE=(.+);OR=([^;]+);BETA=([^;]+);PUBMED=(\d+);STRONGSNP=(\w+);P=(\S+)/ or die "Error: invalid GWAS info found in record: <$_>\n";
		my ($disease, $or, $beta, $pubmed, $strongsnp, $pvalue) = ($1, $2, $3, $4, $5, $6);
		$or eq '.' and $beta eq '.' and next;		#some GWAS records do not have either information in GWAS catalog
		if ($pvalue > 5e-8) {
			$badp++;
			next;
		}
		
		my $newinfo = join ("###", $disease, $or, $beta, $pubmed, $strongsnp);
		
		if ($avgwas->{$snpinfo}) {
			$avgwas->{$snpinfo} .= "\t$newinfo";		#one SNP can be associated with multiple diseases with different OR
		} else {
			$avgwas->{$snpinfo} = $newinfo;
		}
		if ($or ne '.') {
			$all_binary{$disease}++;
		}
		if ($beta ne '.') {
			$all_quant{$disease}++;
		}
	}
	
	for my $key (keys %all_binary) {
		if ($all_quant{$key}) {
			if ($all_quant{$key} > $all_binary{$key}) {
				delete $all_binary{$key};
				print STDERR "WARNING: deleting $key from binary analysis because it was annotated as quant trait as well\n";
			} else {
				delete $all_quant{$key};
				print STDERR "WARNING: deleting $key from quantitative trait analysis because it was annotated as binary trait as well\n";
			}
		}
	}
	
	for my $key (keys %all_binary) {
		my $conf = 1;
		if ($all_binary{$key} > 32) {
			$conf = 5;
		} elsif ($all_binary{$key} > 16) {
			$conf = 4;
		} elsif ($all_binary{$key} > 8) {
			$conf = 3;
		} elsif ($all_binary{$key} > 2) {
			$conf = 2;
		}
		$binary_conf{$key} = $conf;
	}
	for my $key (keys %all_quant) {
		my $conf = 1;
		if ($all_quant{$key} > 32) {
			$conf = 5;
		} elsif ($all_quant{$key} > 16) {
			$conf = 4;
		} elsif ($all_quant{$key} > 8) {
			$conf = 3;
		} elsif ($all_quant{$key} > 2) {
			$conf = 2;
		}
		$quant_conf{$key} = $conf;
	}
	
	print STDERR "NOTICE: A total of ", scalar (keys %$avgwas), " GWAS SNPs were read from $avgwasfile for ", scalar (keys %all_binary), " binary traits and ", scalar (keys %all_quant), " quantitative traits\n";
	$badp and print STDERR "NOTICE: A total of $badp SNPs were skipped that have P>5e-8\n";
	return ($avgwas);
}










=head1 SYNOPSIS

 anno_gwasrisk.pl [arguments] <avgwasfile> <sumfile>

 Optional arguments:
		-h, --help			    print help message
		-m, --man			    print complete documentation
		-v, --verbose			use verbose output
		-json                   print json output 
		--freqcol <string>		header column for allele frequency (default: 1000g2014oct_all)
		--freqfile <file>		a file containing allele frequency information on GWAS catalog

 Function: annotate GWAS risk
 
 Example: anno_gwasrisk.pl 
 
 Version: $Date: 2013-11-12 18:10:49 -0800 (Tue, 12 Nov 2013) $

=head1 OPTIONS

=over 8

=item B<--help>

print a brief usage message and detailed explanation of options.

=item B<--man>

print the complete manual of the program.

=item B<--verbose>

use verbose output.

=item B<--outfile>

the prefix of output file names

=back

=head1 DESCRIPTION

This is a simulation program.


=cut
