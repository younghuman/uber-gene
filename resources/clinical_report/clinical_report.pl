use warnings;
use strict;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use JSON;
my $dirname = dirname(__FILE__);
my %output = ();
our($out_file, $freq_col, $json, $if_genome, $everything, $maf);
GetOptions('out=s'=>\$out_file,  'freq=s'=>\$freq_col, 'json'=>\$json, 
           'genome'=>\$if_genome,'everything'=>\$everything,
           'maf=s'=>\$maf);
$freq_col ||= "all";
$maf ||= 0.01;
@ARGV or pod2usage (-verbose=>0, -exitval=>1, -output=>\*STDOUT);
@ARGV == 1 or die;
my $input_file = $ARGV[0];
open(INPUT, $input_file) or die "Error: Can't read $input_file! ";
open(ACMG, "$dirname/database/DB_ACMG_GENES" ) or die "Error: Can't read $dirname/database/DB_ACMG_GENES";
open(GENE_DISEASE, "$dirname/database/DB_GENE_DISEASE_SCORE") or die "Error: Can't read $dirname/database/DB_GENE_DISEASE_SCORE";
open(RECESSIVE_DISEASE, "$dirname/database/DB_RARE_DISEASE_GENES") or die "Error: Can't read $dirname/database/DB_RARE_DISEASE_GENES";
open(AMBRY, "$dirname/database/DB_AMBRYGEN") or die "Error: Can't read $dirname/database/DB_AMBRYGEN";
if($out_file)
{
open(OUTPUT, ">".$out_file) or die "Error: Can't write to $out_file! ";
}
else{
	*OUTPUT = *STDOUT;
}
my $cmd = "perl $dirname/anno_gwasrisk.pl -freqcol $freq_col -freqfile $dirname/database/DB_GWAS_FREQUENCY $dirname/database/DB_GWAS_CATALOG $input_file ";
   $cmd.= "-json " if ($json);
   $cmd.= "-genome " if($if_genome);
my $gwas_output = `$cmd` ; 
if(not $gwas_output) { die "ERROR: Error generating the GWAS report!\n"; }
if(not $json) { print OUTPUT $gwas_output."\n"; }
        else  { $output{'gwas'} = from_json($gwas_output);    } 


my (%acmg, %gene_disease, %gwas, %gwas_disease, 
    %recessive_disease, %ambry_gene, %ambry_gene_count);
my $i=0;
my $header_line;
my (%clinvar_output, %gwas_output, %acmg_output, %gene_disease_output,
    %recessive_disease_out, %ambry_output, %svm_output);
my %headers = ();

processDatabase();
calculateData();
printResults();
sub calculateData
{
my @clinvar_column = ();
for my $line (<INPUT>){
	$line =~ s/[\r\n]+//g;
	next if($line=~/^\w*$/);
  #Parse the header
	if($i==0) {
		$i++;
		$header_line = $line;
		my @words = split("\t", $line);
		for (my $j=0;$j<@words;$j++)
		{
			if($words[$j] =~ /^1000g(.*?)_$freq_col/i)  { $words[$j] = 'freq'; }
			elsif($words[$j] =~ /^snp/i)    { $words[$j] = 'snp'      ; }
			elsif($words[$j] =~ /^clinvar/i){ 
                 push @clinvar_column,$j;  }
			elsif($words[$j] =~ /^esp6500si_all/i) { $words[$j] = 'esp6500'; }
			elsif($words[$j] =~ /^(snp|dbsnp)/i)   { $words[$j] = 'snp'; }
			elsif($words[$j] =~ /^gene\b/i)        { $words[$j] = 'gene' ;}
			elsif($words[$j] =~ /^func\b/i)        { $words[$j] = 'func' ;}
			elsif($words[$j] =~ /^exonicfunc\b/i)  { $words[$j] = 'exonic_func' ;}
			elsif($words[$j] =~ /^aachange\b/i)    { $words[$j] = 'aachange' ; }
			$headers{$words[$j]} = $j;
		}
	    next;	
	}
 #Process each variant
	else{
		my @words = split("\t", $line);
		my (%vcf_tags, %clinvar_tag);
		my ($chr, $start, $end, $ref, $alt, $func, $gene, $exonic_func,$aachange, $esp, $genome1000,$snp, $svm, $het_hom,$extra)
		= ($words[$headers{'Chr'}],  $words[$headers{'Start'}], $words[$headers{'End'}], $words[$headers{'Ref'}], $words[$headers{'Alt'}],
		   $words[$headers{'func'}],    $words[$headers{'gene'}], $words[$headers{'exonic_func'}], $words[$headers{'aachange'}], 
		   $words[$headers{'esp6500'}],   $words[$headers{'freq'}],    $words[$headers{'snp'}], 
		   $words[$headers{'RadialSVM_score'}] );
		my $clinvar;
		if(@clinvar_column==1) {
			$clinvar = $words[$clinvar_column[0]];
        }
        elsif(@clinvar_column >1){
        	my ($sig, $name, $id) = @clinvar_column[0,1,3];
        	$clinvar = "CLINSIG=".$words[$sig].";CLNDBN=".$words[$name].";CLNACC=".$words[$id];
        }
		for (@words){
			if( /^(hom|het)$/) { $het_hom =$_; }
			if( /^\w+=\S+;\w+=\S+;/) {$extra = $_; }
		}
		$exonic_func= "" if(not $exonic_func);
		$aachange = ""   if(not $aachange);
		$esp   =   0     if(not $esp or $esp =~/^\W+$/);
	    $genome1000 = 0  if(not $genome1000 or $genome1000 =~/^\W+$/);
	    $snp        = "" if(not $snp);
        $svm = 0         if(not $svm or $svm=~/^\W+$/);
		#next if ($func ne 'exonic');
		$extra ||= "";
		my @vcf_tags = split(';', $extra);
           %vcf_tags = ();
        for my $each(@vcf_tags){
                    	my ($name, $content) = split('=', $each);
                    	$vcf_tags{$name} = $content;
        }
        $vcf_tags{'DP'} ||= "NA";
        $vcf_tags{'MQ'} ||= "NA";
        if(not $het_hom) {
        if(not defined $vcf_tags{'AC'}) { die "Error: The vcf tag AC is needed!!\n"; }	
        if    ($vcf_tags{'AC'}=~/^1/) {  $het_hom = 'het';  }
        elsif ($vcf_tags{'AC'}=~/^2/)     {  $het_hom = 'hom';  }
        else   { next; }
           }
     	my @transcripts = split(',', $aachange);
		$transcripts[0] ="" if (not $transcripts[0]);
		my ($gene2, $mRNA, $exon_num, $transcript_change, $protein_change) = split(':', $transcripts[0]);
		my $pos = $chr."\t".$start."\t".$end."\t$ref->$alt";
		$gene2    = "" if(not $gene2);
		$mRNA     = "" if(not $mRNA);
		$exon_num = "" if(not $exon_num);
		$transcript_change = "" if(not $transcript_change);
	    $protein_change =  ""   if(not $protein_change   );
	    
################## Process Clinvar Variants	########################	
		if($svm > 0 and ($exonic_func ne "synonymous SNV") and (!$esp or $esp<$maf)  and (!$genome1000 or $genome1000<$maf))
		{
			$svm_output{$gene}{$pos}{'everything'} = $line;
		}
		
		
		if($clinvar and ($exonic_func ne "synonymous SNV") and (!$esp or $esp<$maf)  and (!$genome1000 or $genome1000<$maf) ) 
		{
			my @tags = split(';', $clinvar);
			%clinvar_tag = ();
			for my $tag (@tags)
			{
				#print $tag."\n";
				my ($name, $content) = split('=', $tag);
				$clinvar_tag{$name} = $content;
          	}
          	
			#print  $clinvar_tag{'CLINSIG'}."\n";
			my @clinsigs = split('\|', $clinvar_tag{'CLINSIG'});
			my @clinnames= split('\|', $clinvar_tag{'CLNDBN'});
			my @clinaccess=split('\|', $clinvar_tag{'CLNACC'});
			for (my $k=0;$k<@clinsigs;$k++) {
				
				if($clinsigs[$k] =~ /^pathogenic$/) { 
					$clinnames[$k] =~s/\\x2c//g;
					$clinnames[$k] =~s/_/ /g;
					
					if (not $clinvar_output{$gene}{$pos}{'disease'})
					{
         			$clinvar_output{$gene}{$pos}{'disease'} = $clinnames[$k] ;
					$clinvar_output{$gene}{$pos}{'dbsnp'} = $snp ;   
                    $clinvar_output{$gene}{$pos}{'maf'}   = $genome1000;
                    $clinvar_output{$gene}{$pos}{'esp'}   = $esp;
                    $clinvar_output{$gene}{$pos}{'svm'}  = $svm;
                    $clinvar_output{$gene}{$pos}{'func'}  = $exonic_func;
                    $clinvar_output{$gene}{$pos}{'het_hom'}  = $het_hom;
                    $clinvar_output{$gene}{$pos}{'clin_access'}  = $clinaccess[$k];
                    $clinvar_output{$gene}{$pos}{'depth'}  = $vcf_tags{'DP'};
                    $clinvar_output{$gene}{$pos}{'quality'}  = $vcf_tags{'MQ'};
                    $clinvar_output{$gene}{$pos}{'mRNA'}  = $mRNA;
                    $clinvar_output{$gene}{$pos}{'exon_num'}  = $exon_num;
                    $clinvar_output{$gene}{$pos}{'transcript_change'}  = $transcript_change;
                    $clinvar_output{$gene}{$pos}{'protein_change'}  = $protein_change;
                    $clinvar_output{$gene}{$pos}{'everything'}  = $line;
					}
					else { $clinvar_output{$gene}{$pos}{'disease'} .= ";".$clinnames[$k] ; 
						   $clinvar_output{$gene}{$pos}{'clin_access'} .=";".$clinaccess[$k];
					}
				 }    
			 } 
			
		  }
################## Process Clinvar Variants	End ########################	
################## Process ACMG Variants Start ########################
          if($acmg{$gene}  and ($func eq 'exonic' or $func eq 'splicing') and ($exonic_func ne "synonymous SNV") 
          and (!$genome1000 or $genome1000<$maf) and(!$esp or $esp<$maf  )  )
          {
          	 $acmg_output{$gene}{$pos}{'acmg'} = $acmg{$gene};
          	 $acmg_output{$gene}{$pos}{'dbsnp'}= $snp;
          	 $acmg_output{$gene}{$pos}{'esp'}   = $esp;
          	 $acmg_output{$gene}{$pos}{'maf'}  = $genome1000;
          	 $acmg_output{$gene}{$pos}{'func'}  = $exonic_func;
          	 $acmg_output{$gene}{$pos}{'svm'}  = $svm;
          	 $acmg_output{$gene}{$pos}{'het_hom'}  = $het_hom;
          	 $acmg_output{$gene}{$pos}{'depth'}  = $vcf_tags{'DP'};
             $acmg_output{$gene}{$pos}{'quality'}  = $vcf_tags{'MQ'};
             $acmg_output{$gene}{$pos}{'mRNA'}  = $mRNA;
             $acmg_output{$gene}{$pos}{'exon_num'}  = $exon_num;
             $acmg_output{$gene}{$pos}{'transcript_change'}  = $transcript_change;
             $acmg_output{$gene}{$pos}{'protein_change'}  = $protein_change;
             $acmg_output{$gene}{$pos}{'everything'} = $line;
          }                 
################## Process ACMG Variants End ########################	
################## Process AmbryGen Variants Start ########################
          if($ambry_gene{$gene}  and ($func eq 'exonic' or $func eq 'splicing') and ($exonic_func ne "synonymous SNV") 
          and (!$genome1000 or $genome1000<$maf) and(!$esp or $esp<$maf  )  )
          {
          	 $ambry_output{$gene}{$pos}{'disease'} = $ambry_gene{$gene};
          	 $ambry_output{$gene}{$pos}{'dbsnp'}= $snp;
          	 $ambry_output{$gene}{$pos}{'esp'}   = $esp;
          	 $ambry_output{$gene}{$pos}{'maf'}  = $genome1000;
          	 $ambry_output{$gene}{$pos}{'func'}  = $exonic_func;
          	 $ambry_output{$gene}{$pos}{'svm'}  = $svm;
          	 $ambry_output{$gene}{$pos}{'het_hom'}  = $het_hom;
          	 $ambry_output{$gene}{$pos}{'depth'}  = $vcf_tags{'DP'};
             $ambry_output{$gene}{$pos}{'quality'}  = $vcf_tags{'MQ'};
             $ambry_output{$gene}{$pos}{'mRNA'}  = $mRNA;
             $ambry_output{$gene}{$pos}{'exon_num'}  = $exon_num;
             $ambry_output{$gene}{$pos}{'transcript_change'}  = $transcript_change;
             $ambry_output{$gene}{$pos}{'protein_change'}  = $protein_change;
             $ambry_output{$gene}{$pos}{'everything'} = $line;
          }                 
################## Process AmbryGen Variants End ########################
	
################## Process Phenolyzer Variants Start ########################
         if($gene_disease{$gene} and ($func=~'exonic' or $func=~'splicing') and ($exonic_func ne "synonymous SNV") and 
           ((!$genome1000 or $genome1000<$maf) and (!$esp or $esp<$maf)))
          {
          	 $gene_disease_output{$gene}{$pos}{'gene_disease'} = $gene_disease{$gene};
          	 $gene_disease_output{$gene}{$pos}{'dbsnp'}= $snp;
          	 $gene_disease_output{$gene}{$pos}{'esp'}   = $esp;
          	 $gene_disease_output{$gene}{$pos}{'maf'}  = $genome1000;
          	 $gene_disease_output{$gene}{$pos}{'func'}  = $exonic_func;
          	 $gene_disease_output{$gene}{$pos}{'svm'}  = $svm;
          	 $gene_disease_output{$gene}{$pos}{'het_hom'}  = $het_hom;
          	 $gene_disease_output{$gene}{$pos}{'depth'}  = $vcf_tags{'DP'};
             $gene_disease_output{$gene}{$pos}{'quality'}  = $vcf_tags{'MQ'};
             $gene_disease_output{$gene}{$pos}{'mRNA'}  = $mRNA;
             $gene_disease_output{$gene}{$pos}{'exon_num'}  = $exon_num;
             $gene_disease_output{$gene}{$pos}{'transcript_change'}  = $transcript_change;
             $gene_disease_output{$gene}{$pos}{'protein_change'}  = $protein_change;
             $gene_disease_output{$gene}{$pos}{'everything'} = $line;
          }
################## Process Phenolyzer Variants End ########################

################## Process Truesight Recessive Disease Variants Start ########################
         if($recessive_disease{$gene}  and ($func=~'exonic' or $func=~'splicing') and ($exonic_func ne "synonymous SNV") 
          and (!$genome1000 or $genome1000<$maf) and (!$esp or $esp<$maf  )  )
          {
          	 $recessive_disease_out{$gene}{$pos}{'disease'} = $recessive_disease{$gene}{'disease'};
             $recessive_disease_out{$gene}{$pos}{'omim'} = $recessive_disease{$gene}{'omim'};          	
          	 $recessive_disease_out{$gene}{$pos}{'dbsnp'}= $snp;
          	 $recessive_disease_out{$gene}{$pos}{'esp'}   = $esp;
          	 $recessive_disease_out{$gene}{$pos}{'maf'}  = $genome1000;
          	 $recessive_disease_out{$gene}{$pos}{'func'}  = $exonic_func;
          	 $recessive_disease_out{$gene}{$pos}{'svm'}  = $svm;
          	 $recessive_disease_out{$gene}{$pos}{'het_hom'}  = $het_hom;
          	 $recessive_disease_out{$gene}{$pos}{'depth'}  = $vcf_tags{'DP'};
             $recessive_disease_out{$gene}{$pos}{'quality'}  = $vcf_tags{'MQ'};
             $recessive_disease_out{$gene}{$pos}{'mRNA'}  = $mRNA;
             $recessive_disease_out{$gene}{$pos}{'exon_num'}  = $exon_num;
             $recessive_disease_out{$gene}{$pos}{'transcript_change'}  = $transcript_change;
             $recessive_disease_out{$gene}{$pos}{'protein_change'}  = $protein_change;
             $recessive_disease_out{$gene}{$pos}{'everything'} = $line;
          }
################## Process Recessive Disease Variants End ########################

	}
	}
}

sub printResults
{
#################################  Print out results   ##################################
if(not $json)
{ 
if($everything) {  print OUTPUT join("\t", ("Gene", $header_line))."\n";  }
print OUTPUT "#Disease variants reported in Clinvar\n";
if (not $everything)   {    print OUTPUT join("\t", qw/Gene Chr Start End Allele ExonicFunc Depth Status mRNA Exon mRNA_change Protein_change 
                            dbSNPtag 1000genome_Frequency espFrequency Disease ClinVarAccess/)."\n" ;  }   
                          
for  my $gene (keys %clinvar_output){
	 print OUTPUT $gene."\t";
	 my $flag = 0;
	 for my $pos (keys %{$clinvar_output{$gene}})
	 {
	 	print OUTPUT "\t" if($flag ==1); 
	 	if(not $everything){
	 	print OUTPUT $pos
	 	."\t".$clinvar_output{$gene}{$pos}{'func'}
	 	."\t".$clinvar_output{$gene}{$pos}{'depth'}
	 	."\t".$clinvar_output{$gene}{$pos}{'het_hom'} 
	 	."\t".$clinvar_output{$gene}{$pos}{'mRNA'}
	 	."\t".$clinvar_output{$gene}{$pos}{'exon_num'}
	 	."\t".$clinvar_output{$gene}{$pos}{'transcript_change'}
	 	."\t".$clinvar_output{$gene}{$pos}{'protein_change'}
	 	."\t".$clinvar_output{$gene}{$pos}{'dbsnp'}
	 	."\t".$clinvar_output{$gene}{$pos}{'maf'}
	 	."\t".$clinvar_output{$gene}{$pos}{'esp'}
	 	."\t".$clinvar_output{$gene}{$pos}{'disease'}
	 	."\t".$clinvar_output{$gene}{$pos}{'clin_access'}."\n";
	 	}
	 	else{
	 		print OUTPUT $clinvar_output{$gene}{$pos}{'everything'}."\n";
	 	}
	 	$flag=1;
    }
}


print OUTPUT "\n#Rare variants in ACMG incidental genes\n";
if(not $everything)  {   print OUTPUT join("\t", qw/Gene Chr Start End Allele ExonicFunc Depth Status mRNA Exon mRNA_change 
                                            Protein_change dbSNPtag 1000genome_Frequency espFrequency Disease OMIM Pubmed/)."\n"; }
for  my $gene (keys %acmg_output){
	 print OUTPUT $gene."\t";
	 my $flag = 0;
	 for my $pos (keys %{$acmg_output{$gene}})
	 {
	 	print OUTPUT "\t" if($flag ==1); 
	 	if(not $everything){
	 	print OUTPUT $pos
	 	."\t".$acmg_output{$gene}{$pos}{'func'}
	 	."\t".$acmg_output{$gene}{$pos}{'depth'}
	 	."\t".$acmg_output{$gene}{$pos}{'het_hom'}
	 	."\t".$acmg_output{$gene}{$pos}{'mRNA'}
	 	."\t".$acmg_output{$gene}{$pos}{'exon_num'}
	 	."\t".$acmg_output{$gene}{$pos}{'transcript_change'}
	 	."\t".$acmg_output{$gene}{$pos}{'protein_change'}
	 	."\t".$acmg_output{$gene}{$pos}{'dbsnp'}
	 	."\t".$acmg_output{$gene}{$pos}{'maf'}
	 	."\t".$acmg_output{$gene}{$pos}{'esp'}
	 	."\t".$acmg_output{$gene}{$pos}{'acmg'}
        ."\n";
	 	}
	 	else{
	 		print OUTPUT $acmg_output{$gene}{$pos}{'everything'}."\n";
	 	}
	 	$flag=1;
	 	
     }
}

print OUTPUT "\n#Rare variants in AmbryGen\n";
if(not $everything)  {  print OUTPUT join("\t", qw/Gene Chr Start End Allele ExonicFunc Depth Status 
             mRNA Exon mRNA_change Protein_change dbSNPtag 1000genome_Frequency espFrequency Disease/)."\n";  }
for my $filter (keys %ambry_gene_count){
	print_ambry($filter);
}
sub print_ambry{
	my ($filter) = $_[0];
	if($filter =~ /^cancer$/i) {   print OUTPUT "##AmbryGen CANCER panel: $ambry_gene_count{cancer} genes\n"; }
	elsif($filter =~ /^intellectual disability$/i)
	{  print OUTPUT "##AmbryGen INTELECTUAL DISABILITY panel: $ambry_gene_count{'intellectual disability'} genes\n";   }
	elsif($filter =~ /^misc$/i)
	{  print OUTPUT "##AmbryGen RHYTHMFIRST AND RHYTHMNEXT panel: $ambry_gene_count{misc} genes\n";   }
	for my $gene (keys %ambry_output){ 
	 if(not %{$ambry_output{$gene}}) { next; }
	 else { 
	 	my @pos = keys %{$ambry_output{$gene}};
	 	next if ($ambry_output{$gene}{$pos[0]}{'disease'} ne $filter and $filter ne "misc");
	 	next if ($ambry_output{$gene}{$pos[0]}{'disease'}=~/^(cancer|intellectual disability)$/i
	 	         and $filter eq "misc");
	 }
	 print OUTPUT $gene."\t";
	 my $flag = 0;
	 for my $pos (keys %{$ambry_output{$gene}})
	 {
	 	print OUTPUT "\t" if($flag ==1); 
	 	if(not $everything){
	 	print OUTPUT $pos
	 	."\t".$ambry_output{$gene}{$pos}{'func'}
	 	."\t".$ambry_output{$gene}{$pos}{'depth'}
	 	."\t".$ambry_output{$gene}{$pos}{'het_hom'}
	 	."\t".$ambry_output{$gene}{$pos}{'mRNA'}
	 	."\t".$ambry_output{$gene}{$pos}{'exon_num'}
	 	."\t".$ambry_output{$gene}{$pos}{'transcript_change'}
	 	."\t".$ambry_output{$gene}{$pos}{'protein_change'}
	 	."\t".$ambry_output{$gene}{$pos}{'dbsnp'}
	 	."\t".$ambry_output{$gene}{$pos}{'maf'}
	 	."\t".$ambry_output{$gene}{$pos}{'esp'}
	 	."\t".$ambry_output{$gene}{$pos}{'disease'}
        ."\n";
	 	}
	 	else{
	 	print OUTPUT $ambry_output{$gene}{$pos}{'everything'}."\n";
	 	}
	 	$flag=1;
     }
}
}

print OUTPUT "\n#Rare deleterious variants in disease genes reported in OMIM, Orphanet, GeneReviews\n";
print OUTPUT join("\t", qw/Gene Chr Start End Allele ExonicFunc Depth Status mRNA Exon mRNA_change Protein_change 
                           dbSNPtag 1000genome_Frequency espFrequency Disease Disease_ID/)."\n";
for my $gene (keys %gene_disease_output){
	
	 print OUTPUT $gene."\t";
	 my $flag = 0;
	 for my $pos (keys %{$gene_disease_output{$gene}})
	 {
	 	if(not $everything)
	 	{
	 	print OUTPUT "\t" if($flag ==1); 
	 	print OUTPUT $pos
	 	."\t".$gene_disease_output{$gene}{$pos}{'func'}
	 	."\t".$gene_disease_output{$gene}{$pos}{'depth'}
	 	."\t".$gene_disease_output{$gene}{$pos}{'het_hom'}
	 	."\t".$gene_disease_output{$gene}{$pos}{'mRNA'}
	 	."\t".$gene_disease_output{$gene}{$pos}{'exon_num'}
	 	."\t".$gene_disease_output{$gene}{$pos}{'transcript_change'}
	 	."\t".$gene_disease_output{$gene}{$pos}{'protein_change'}
	 	."\t".$gene_disease_output{$gene}{$pos}{'dbsnp'}
	 	."\t".$gene_disease_output{$gene}{$pos}{'maf'}
	 	."\t".$gene_disease_output{$gene}{$pos}{'esp'}
	 	."\t".$gene_disease_output{$gene}{$pos}{'gene_disease'}
	 	."\n";
	 	}	
	 	else{
	 	print OUTPUT $gene_disease_output{$gene}{$pos}{'everything'}."\n";
	 	}
     	$flag=1;
     }
}

print OUTPUT "\n#Carrier Status\n";
if(not $everything)  {  print OUTPUT join("\t", qw/Gene Chr Start End Allele ExonicFunc Depth Status mRNA 
                  Exon mRNA_change Protein_change dbSNPtag 1000genome_Frequency espFrequency Disease OMIM/)."\n"; }
for my $gene (keys %recessive_disease_out){	
	 print OUTPUT $gene."\t";
	 my $flag = 0;
	 for my $pos (keys %{$recessive_disease_out{$gene}})
	 {
	 	print OUTPUT "\t" if($flag ==1); 
	 	if(not $everything){
	 	print OUTPUT $pos
	 	."\t".$recessive_disease_out{$gene}{$pos}{'func'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'depth'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'het_hom'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'mRNA'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'exon_num'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'transcript_change'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'protein_change'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'dbsnp'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'maf'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'esp'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'disease'}
	 	."\t".$recessive_disease_out{$gene}{$pos}{'omim'}
	 	."\n";
	 	}
	 	else{
	 	print OUTPUT $recessive_disease_out{$gene}{$pos}{'everything'}."\n";
	 	}
	 	$flag=1;
     }
  }
  
  print OUTPUT "\n#Predicted deleterious variants\n";
  for my $gene (keys %svm_output){	
	 print OUTPUT $gene."\t";
	 my $flag = 0;
	 for my $pos (keys %{$svm_output{$gene}})
	 {
	 	print OUTPUT "\t" if($flag ==1); 
	 	print OUTPUT $svm_output{$gene}{$pos}{'everything'}."\n";
	 	$flag=1;
     }
  }

}   #if(not $json)

else{
	$output{'clinvar'} = \%clinvar_output;
	$output{'acmg'}    = \%acmg_output;
	$output{'ambry'}  =  \%ambry_output;
    $output{'phenolyzer'}    = \%gene_disease_output;
	$output{'carrier'} = \%recessive_disease_out;
	$output{'svm'}     = \%svm_output;
	print OUTPUT to_json(\%output);
}

}

###########################  Output End  ###############################

sub processDatabase {
	################## Process ACMG Database ########################	
	my $last_record ;
	for my $line (<ACMG>){
		$line =~ s/[\n\r]//g;
		$line =~ s/"//g;
		my @words = split("\t", $line);
		my ($disease, $omim, $pubmed, $gene) = @words[0,1,2,4];
		if($disease)
		{
		$acmg{$gene} = join("\t", ($disease,$omim, $pubmed));
		$last_record = join("\t", ($disease,$omim, $pubmed));
		}
		else{
		$acmg{$gene} = $last_record;
		}
		
	}
	################## Process ACMG Database End    ########################
	################## Process Phenolyzer Database  ########################
	my $i=0;
	my %repeat_check = ();
	for my $line (<GENE_DISEASE>)
	{
		if($i==0) {$i++; next;}
		$line =~ s/[\n\r]//g;
		$line =~ s/"//g;
		my ($gene, $disease, $disease_id, $score, $source) = split("\t", $line);
		if( ($source!~ /^(OMIM|ORPHANET|GENE_REVIEWS)$/ or $score < 1) ) {next;}
		if( $source eq "GENE_REVIEWS") {
			if($disease=~/^(.+) \w{1-3}?$/)
			{
				$disease = $1;
			}
		}
		my @genes = split(',', $gene);
		$gene = $genes[0];
		if(not $gene_disease{$gene}) 
		{
         	$gene_disease{$gene} = join("\t", ($disease, $disease_id));
		}
		else
		{
			my ($old_disease, $old_disease_id) = split("\t", $gene_disease{$gene});
			$disease_id .= ", ".$old_disease_id;	
            $disease .= ";".$old_disease; 
            $gene_disease{$gene} = join("\t", ($disease, $disease_id));
      	}
 	}
    ################## Process Phenolyzer Database  End ########################
	################## Process Recessive Diseases       ########################
 	for my $line (<RECESSIVE_DISEASE>){
 		$line =~ s/[\r\n]+//g;
 		my ($omim, $disease, $gene) = split("\t", $line);
 		$disease =~s/"//g;
 		$omim = "OMIM:$omim";
 		if(not $recessive_disease{$gene}{'disease'})
 		{
 		$recessive_disease{$gene}{'disease'} = $disease;
 		$recessive_disease{$gene}{'omim'} = $omim;
 		}
 		else{
 		$recessive_disease{$gene}{'disease'} .=";".$disease;
 		$recessive_disease{$gene}{'omim'} .= ", ".$omim;
 			
 		}
 	}
 	#  $flag is the current processed disease type
 	my $flag = 0;
 	
 	for my $line (<AMBRY>){
 		$line =~ s/[\r\n]+//g;
 		next if not $line;
 		if($line =~ /^##Misc##$/i) {
 			$flag = 'misc';
 			next;
 		}
 		elsif($line=~ /^##(.*)##/) {
 			$flag = lc $1;
 			next;
 		}
 		if($flag eq 'misc'){
 			my ($disease, $gene) = split("\t", $line);
 			my @genes = split(/\s*,\s*/,$gene);
 			for my $gene (@genes) {
 			   $ambry_gene{$gene} = $disease ;
 			   if(not $ambry_gene_count{'misc'}) { 
 				     $ambry_gene_count{'misc'} = 1 ; 
 			   }
 		       else  {  $ambry_gene_count{'misc'}++ ; }
 			}
 			 
 		}
 		elsif($flag){
 			my @genes = split('\/', $line);
 			for my $gene (@genes){
 			    $ambry_gene{$gene} = $flag;
 			}
 			if(not $ambry_gene_count{$flag}) {
 				$ambry_gene_count{$flag} = 1;
 			}
 			else {  $ambry_gene_count{$flag}++; }
 			
 		}
 	} 		
}



=head1 SYNOPSIS

 perl clinical_report.pl <annotated_file>  [arguments]
     arguments:
           -out           The file to save the results
           -freq          The coloumn name for the variant frequency
           -json          Output json instead of text
           -genome        If the input file covers the whole genome
           -everything    Output everything in the ANNOVAR annotation for rare variants
=cut
