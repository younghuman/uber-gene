var User = require('../models/user');
module.exports = function(io, clients){
	//Open a chat socket
	io.of('/chat').
		        on('connection', function (socket) {
		        if(!socket.request.user) return;
		        var username = socket.request.user.data.name;
		        var email = socket.request.user.username;
		        var id    = socket.request.user._id;
	            console.log(username +' connected to as a Chat socket!');
	            clients[email] = socket;
	          //Chat message received
	            socket.on('chat message', function(msg){
	            	io.of('/chat').emit('chat message',{'username':username,'msg':msg,
	            		        'color':socket.request.color} );
	            	   }); 
	         
	          //Disconnection
	            socket.on('disconnection', function(socket) { 
	           	console.log(username+' disconnected from chat socket');
	           	if(clients.email) delete clients.email;
	            });
	      });     //--------------Chat socket ends-------------
}