var User = require('../models/user');
var compareDate = function (a, b){
	var a_date = new Date(a.date);
	var b_date = new Date(b.date);
	if(a_date < b_date) return 1;
	else if(a_date>b_date) return -1;
	else return 0;
}

module.exports = function(io, clients){
	 io.of("/notice")
	    .on("connection", function(socket)  {
	     if(!socket.request.user) return;
	//Initiate user profile
	     var username = socket.request.user.data.name;
	     var email = socket.request.user.username;
	     var id    = socket.request.user._id;
		 console.log(username +' connected to as an Notice socket!');	
    //'update notice' is for the Notice section  		 
		 socket.on("update notice", function(){
			 var user = socket.request.user;
			 var data  = user.data;
			 if(data==null) return;
	         var friendRequest = data.friendRequest;
	         var friendSend   =  data.friendSend;
	         var systemLogs   =  data.systemLogs;
			 var notices = [];
			 for (var id in friendRequest){
				  var notice = {subjectName:friendRequest[id].name,
					        subjectEmail:friendRequest[id].email,
					        subjectId: id,
					        type:"friendRequest",
					        date:friendRequest[id].date,
					        message:friendRequest[id].message,
					        status:friendRequest[id].status} 
				 notices.push(notice);
			 }
			 for (var id in friendSend){
				  var notice = {ToName:friendSend[id].name,
					        ToEmail:friendSend[id].email,
					        date:friendSend[id].date,
					        ToId: id,
					        type:"friendSend",
					        status:friendSend[id].status} 
			     notices.push(notice);
              }
			 for (var id in systemLogs){
				 var notice = {
					        date:systemLogs[id].date,
					        Id:id,
					        type:"systemLogs",
					        message:systemLogs[id].message
                          } 
				 notices.push(notice);
			 }
			 //console.log(notice);
		     if(notices){
		    	 notices.sort(compareDate);
		    	 socket.emit("update notice", notices);
		     }
		 });
	//'check notice' is for the 'new' badge
		 socket.on("check notice", function(){
			 var user = socket.request.user;
			 var data  = user.data;
	         var friendRequest = data.friendRequest;
	         var friendSend    = data.friendSend;
	         var systemLogs    = data.systemLogs;
	         var checkStatus = function(notices) {
	        	 for (var id in notices){
	                 if(!notices[id].seen || notices[id].status=="unseen" ){
	                	 socket.emit("check notice", true);
	                	 return true;
	                 }
				 }
	        	 return false;
	         }
			 if(checkStatus(friendRequest))  socket.emit("check notice", true);
			 else if(checkStatus(friendSend))    socket.emit("check notice", true);
			 else if(checkStatus(systemLogs))    socket.emit("check notice", true);  
			 else socket.emit("check notice", false);
    	 });
	//'see notice' is for the 'new' badge
		 socket.on("see notice", function(){
			 var user = socket.request.user;
			 var data  = user.data;
			 if(data==null) return;
	         var friendRequest = data.friendRequest;
	         var friendSend    = data.friendSend;
	         var systemLogs    = data.systemLogs;
	         var checkStatus = function(notices) {
	        	 for (var id in notices){
                       notices[id].seen=true;
				 }
	         }
			 checkStatus(friendRequest);
			 checkStatus(friendSend);
			 checkStatus(systemLogs);
		     user.update({"data.friendRequest":friendRequest, 
		    	          "data.friendSend":friendSend, 
		    	          "data.systemLogs":systemLogs},null, function(err){
		    	         if(err) console.log(err);	 
		    	         }); 
    	 });
		  
    //'respond request' is for the response to the notices
		 socket.on('respond request', function(message){
			 var user = socket.request.user;
			 var data  = user.data;
	         var id = user._id;
	         var email = user.username;
	         var name = user.data.name;
	         var date = new Date();
    	         date = date.toLocaleTimeString() +" "+ date.toLocaleDateString();
			 if(!user||!data) return;
//---------------------------Respond to friend request, either Accept or Reject------------------------------------
			 if(message.type=="friendRequest"){
			     //Update the user who just accepts		
                 //If the user accepts the friend request  	
					 if(!user.data.friendRequest|| !user.data.friendRequest[message.id]){
						  socket.emit('respond request');
						  return;
					}
					 if(message.response=="accept")   
					 {   
                        if(!user.data.friends)     user.data.friends = {};	
						user.data.friendRequest[message.id].status = "accepted";
						user.data.friendRequest[message.id].date = date;
                     	user.data.friends[message.id]={email:message.email, name:message.name};
						user.update({'data':user.data}, null, function(err){
							if(err) console.log(err);
							socket.emit('respond request');
							console.log("[FriendAdded]"+email+" and "+message.email+" are friends now!");
						})
				//Update the user who sends the friend request
					 User.findOne({username:message.email}, function(err, user){
						 if(err) console.log(err);
						 if(user)
					     {
						 if(!user.data.friendSend || !user.data.friendSend[id]) 
						 { socket.emit('respond request');  return;  }
						 if(!user.data.friends) user.data.friends = {};
                         user.data.friendSend[id].status = "accepted";
                         user.data.friendSend[id].date = date;
						 user.data.friends[id] = {email:email, name:name }
						 user.update({'data':user.data}, null, function(err){
							 if(err) console.log(err);
							 socket.emit('respond request');
						 })
					     }
					 });
				   }// if(message.response=="accept")
			     //If the user accepts the friend request
				  else if(message.response=="reject")
				  {
					  user.data.friendRequest[message.id].status = "rejected";
					  user.data.friendRequest[message.id].date   = date;
					  user.update({'data.friendRequest':user.data.friendRequest}, null, function(err){
						  if(err) console.log;
                          socket.emit('respond request');
                          console.log("[FriendRejected]"+email)
					  })
					 User.findOne({username:message.email},function(err, user){
						 if(err) console.log(err);
						 if(!user.data.friendSend || !user.data.friendSend[id]) 
						 { socket.emit('respond request');  return;  }
						 user.data.friendSend[id].status = "rejected";	
						 user.data.friendSend[id].date   = date;
						 user.update({'data.friendSend':user.data.friendSend}, null, function(err){
						     if(err) console.log(err);
						     socket.emit('respond request');
						 });
						})
				  }//if(message.response=="reject")
				  else if(message.response=="delete")
				  {
					  if(user.data.friendRequest[message.id])  delete user.data.friendRequest[message.id];
					  user.update({'data.friendRequest':user.data.friendRequest},null, function(err){
						  if(err) console.log(err);
						  socket.emit('respond request');
					  })
				  }
		       }// if(message.type=="friendRequest")
		// Respond to friend request End
        //---------------------------Respond to friend Send Start-----------------------------------------------------------     
			 else if(message.type=="friendSend") {
            	 if(!user.data.friendSend || !user.data.friendSend[message.id]) {
            		 socket.emit('respond request');
            		 return;
            	 }
            	 if(message.response=="recall"){
            		 delete user.data.friendSend[message.id];
            		 if(user.data.friends && user.data.friends[message.id]){
            			 delete user.data.friends[message.id];
            		 }
            		 user.update({'data.friends':user.data.friends, 'data.friendSend':user.data.freindSend}, null, function(err){
            			 if(err) console.log(err);
            			 console.log("[FriendRequestRecal]"+name+" recalled his request to "+message.name);
            			 socket.emit('respond request');
            		 })
            		 User.findOne({username:message.email}, function(err, user){
            			 if(err) console.log(err);
            			 if(!user.data.friendRequest || !user.data.friendRequest[id]) 
    					 { socket.emit('respond request');  return;  }
                		 delete user.data.friendRequest[id];
                		 if(user.data.friends && user.data.friends[id]){
                			 delete user.data.friends[id];
                		 }
                		 user.update({'data.friends':user.data.friends, 'data.friendRequest':user.data.friendRequest}, null, function(err){
                			 if(err) console.log(err);
                			 socket.emit('respond request');
                		 })	 
            		 })
            	 }
            	//delete the notice message
            	 else if(message.response=="delete")
				 {
					  if(user.data.friendSend[message.id]) delete user.data.friendSend[message.id];
					  user.update({'data.friendSend':user.data.friendSend},null, function(err){
						  if(err) console.log(err);
						  socket.emit('respond request');
					  })
				 }
			 }
			 //---------------------------Respond to friend Send End-----------------------------------------------------------     
				
			 else if(message.type=="systemLogs") {
				 if(!user.data.systemLogs|| !user.data.systemLogs[message.id]){
					  socket.emit('respond request');
					  return;
				 }
				 if(message.response=="delete")
			     {
					 if(user.data.systemLogs[message.id]) delete user.data.systemLogs[message.id];
					 user.update({'data.systemLogs':user.data.systemLogs},null, function(err){
						 if(err) console.log(err);
						 socket.emit('respond request');
					 })				 
			     }
			 }
			
		 });
    //Disconnection
		 socket.on('disconnection', function(socket) { 
	        	console.log(username+' disconnected from Notice socket');
	        	if(clients.email) delete clients.email;
	         });
		 
	 });
}