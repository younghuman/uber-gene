var User = require('../models/user');
var message_string = {
		            warning:'warning',
		            info:'info',
		            danger:'danger',
		            primary:'primary',
		            success:'success'
                }
module.exports = function(io,clients) {
	io.of('/friend').
    on('connection', function (socket) {
   if(!socket.request.user) return;
    var username = socket.request.user.data.name;
    var email = socket.request.user.username;
    var id    = socket.request.user._id;
    clients[email]=socket;
    console.log(username +' connected as a Friend socket!');
    //Search Friend
    socket.on('search friend', function(email){       
    	User.find({ $or:
    		[{'username':email }, {'data.name':email}] }
    	    , function(err, user){
    	    if(err) console.log(err);
    		if(user) {
    			var users = [];
    			for (var i=0;i<user.length;i++){
    				users.push({'email':user[i].username, 
    					         'name':user[i].data.name,
    					          'id': user[i]._id  }); 
    			}
    			socket.emit('search friend', users);
    		}
    		else{
    			socket.emit('search friend', null);
    		}
    	});
    });  //-----------------Search Friend Ends---------------
    //delete friend
    socket.on('delete friend', function(info){
    	var user_send = socket.request.user;
   	    var date = new Date();
            date = date.toLocaleTimeString() +" "+ date.toLocaleDateString();
        if(!user_send.data.friends || !user_send.data.friends[info.id]){
        	console.log("[DeleteFriend]Friend does not exist anymore!");
        	socket.emit('delete friend', ['[Warning]Friend'+info.email+' does not exist anymore', 'warning']);
        	return;
        }
   	    if(info.id==user_send._id) {
		 console.log("[DeleteFriend]Can't delete yourself!");
		 socket.emit('delete friend', ["[Warning]Can't delete yourself", 'warning']);
		 return;
	    }
   	    User.findOne({'username':info.email}, function(err, user){
   	    	if(err) console.log(err);
   	    	if(user.data.friends && user.data.friends[user_send._id])
   	    	delete user.data.friends[user_send._id];
   	    	user.update({'data.friends':user.data.friends},null, function(err){
   	    		if(err) console.log(err);
   	    		delete user_send.data.friends[info.id];
   	    		user_send.update({'data.friends':user_send.data.friends}, null, function(err){
   	    			if(err) console.log(err);
   	    		console.log("[DeleteFriend]"+user_send.username+" and "+info.email+" are not " +
   	    				"friends any more!");
   	    		socket.emit('delete friend',["[Success]Friend "+info.email+" has been deleted.","success"]);
   	    		});
   	    	})
   	    	
   	    })
   	 
    })
    //---------------------------Delete friend End--------------------
    
//Add Friend 
    socket.on('add friend', function(info){
      //Find the requested user 
         var user_send = socket.request.user;
    	 var date = new Date();
    	     date = date.toLocaleTimeString() +" "+ date.toLocaleDateString();
    	 if(user_send.data.friends && user_send.data.friends[info.id]) 
    	 {
    		  console.log("[AddFriend]Friend already exists!");
    		  socket.emit('add friend', ['[Warning]Friend '+info.email+ ' already exists!','warning']);
    		  return;
    	 }
    	 if(info.id==user_send._id) {
    		 console.log("[AddFriend]Can't add yourself as friend!");
    		 socket.emit('add friend', ["[Warning]Can't add yourself as friend", 'warning']);
    		 return;
    	 }
    	 User.findOne({'username':info.email},
          function(err, user) {
    		if(err) conole.log(err);
    		if(user) {
    			if(user)
    			//update the user who sends the request
    			if(!user_send.data.friendSend)  user_send.data.friendSend = {}; 
    			user_send.data.friendSend[user._id] = {
    					  name:user.data.name,
    					  email:user.username,
    					  status:'unseen',
    					  date:date
    			}
                user_send.update({"data.friendSend":user_send.data.friendSend }, null, function(err){
                	if(err){
                		console.log(err);
                		console.log("[AddFriend]Friend request save failure!");
                	}
                });
    		
    			//update the user who receives the request
    			if(!user.data.friendRequest) user.data.friendRequest = {};
    			user.data.friendRequest[id]={
    					 message:info.msg, 
    					 name:username, 
    					 email:email, 
    					 status:'unseen', 
    					 date:date
    					 }  
    			user.update({"data.friendRequest":user.data.friendRequest},null,function(err){ 
    				if(err) {
    					console.log(err);
    					console.log("[AddFriend]Friend request save failure!");
    					socket.emit('add friend', null);
    				}
    				else{
    				console.log("[AddFriend]"+ email+" wants to join "+info.email +" as friend!");
        	        socket.emit('add friend', ['[Success]Friend request to ' +info.email+ ' sent successful!','info']);	
    				}
    			})	
    			}//if(user)
    		else{
    			console.log("[AddFriend] Failed as "+info.email+ " doesn't exist any more!");
    			socket.emit('add friend', null);
    		}
    	});  //-----------User.findOne ends-------------
    	 
    	 
    });  //----------add friend ends------------------
    	//Disconnection
         socket.on('disconnection', function(socket) { 
        	console.log(username+' disconnected from friend socket');
        	if(clients.email) delete clients.email;
         });
    });   //-------------Friend socket ends---------------
	
	return io;
}