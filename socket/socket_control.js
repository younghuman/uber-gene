var User = require('../models/user');
var colorArray = ["#efaa55","#f3f333","#77bb44","#66baa9", "#222222"];
module.exports = function(sessionMiddleware, io) {
	 io.use(function(socket, next) {
		   sessionMiddleware(socket.request, socket.request.res, next);  
	   });
	 io.use(function(socket, next) {
		  if(socket.request.session.passport) 
		  {
		  var id = socket.request.session.passport.user;
		  } 
		  User.findById(id, function(err, user) {
		        if(err) next(new Error('Authentication error'));
		        console.log("Authenticated user!");
		        socket.request.user = user;
		        socket.request.color=colorArray[Math.floor(Math.random()*colorArray.length)];
		        next();
		    });
      });
//clients save all the connected sockets with emails as keys
	var clients_chat ={};
	var clients_friend = {};
	var clients_notice = {};
	require("./socket_chat")(io,clients_chat);
    //Open friend socket
    require("./socket_friend")(io,clients_friend);
    require("./socket_notice")(io, clients_notice);
	
}; 


