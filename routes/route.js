var signup = require('../controllers/signup.js');
var upload = require('../controllers/upload.js');
var friends= require('../controllers/friends.js');
var download = require("../controllers/download.js");
module.exports = function(app,passport)
{
var TITLE = app.get("settings").title;
app.route("/")
 .get(function(req, res) {
	  if(req.isAuthenticated())
	    {
		  //console.log(req.session);
		  res.render("index", {
			  user:req.user.data.name,
              title:TITLE});
	    }
	  else
		{
		res.render("index", {title:TITLE, 
			                 signinMessage: req.flash("signinMessage"),
			                 signupMessage: req.flash("signupMessage")}); 
		}
 }) ;
//Sign in
app.route("/signin")
  .post(passport.authenticate("local-signin", {
	    successRedirect : '/profile', // redirect to the secure profile section
		failureRedirect : '/', // redirect back to the signup page if there is an error 
		failureFlash : true // allow flash messages
	   
  }));

//Sign up
app.route("/signup")
  .post(signup);

//Profile
app.route("/profile")
  .get(isLoggedIn, function(req, res) {
	  res.render("profile", {user: req.user,
		                     title: TITLE,
		                     signupMessage: req.flash("signupMessage")});
  });

//Personal pages
app.get("/profile/:page",isLoggedIn,friends.findRandomPeople,function(req,res){
	  res.render("profile", {
		  user: req.user,
		  title: TITLE,
		  page:req.params.page,
		  userlist:req.userlist
	  })   
});
app.get("/download/me-report",isLoggedIn,download.PersonalReport,function(req,res,next){
	res.redirect("/profile/me/health");
})

app.get("/profile/:page/friends",isLoggedIn,friends.findRandomPeople,function(req,res, next){
	res.render("profile", {
	  user: req.user,
	  title: TITLE,
	  page:req.params.page,
	  subpage:'friends',
	  userlist:req.userlist
  })  
});
app.get("/profile/:page/:subpage",isLoggedIn,function(req,res, next){
	     res.render("profile", {
		  user: req.user,
		  title: TITLE,
		  page:req.params.page,
		  subpage:req.params.subpage,
	  })  
});


//Page after upload
app.route("/upload")
.get(isLoggedIn, function(req,res){
 res.render("profile", {
		   user:req.user, 
		   title: TITLE,
		   page:"upload"
	        })	
      })
.post( isLoggedIn, upload.multer,upload.process);

//logout
app.route("/logout")
  .get(function(req,res) {
	   req.logout();
	   res.redirect("/");
  });
} 

var isLoggedIn = function(req, res, next){
	if(req.isAuthenticated()) return next();
	res.redirect("/");
} 

