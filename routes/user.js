module.exports = function(passport)
{
var express = require('express');
var router = express.Router();
var userController = require('../controllers/user');

router.route('/')
   .post(userController.postUsers)
   .get(passport.authenticate("local-signin", {
	   session:true
   }), userController.getUsers);

}