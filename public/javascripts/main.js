jQuery(document).ready(function($){
	var $form_modal = $('.cd-user-modal'),
		$form_login = $form_modal.find('#cd-login'),
		$form_signup = $form_modal.find('#cd-signup'),
		$form_reset  = $form_modal.find(' #cd-reset-password');
		$form_forgot_password = $form_modal.find('#cd-reset-password'),
		$form_modal_tab = $('.cd-switcher'),
		$tab_login = $form_modal_tab.children('li').eq(0).children('a'),
		$tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
		$forgot_password_link = $form_login.find('.cd-form-bottom-message a'),
		$back_to_login_link = $form_forgot_password.find('.cd-form-bottom-message a'),
		$main_nav = $('.inner.cover a');
     //validate username
	    function validateUsername(username) {
		    var error = "";
		    var illegalChars = /\W/; // allow letters, numbers, and underscores
    	    if (username == "") {
		        error = "You didn't enter a username.\n";
		    } else if ((username.length < 3) || (username.length > 15)) { 
		        error = "The username should be between 3 and 15 characters.\n";
		    } else if (illegalChars.test(username)) { 
		        error = "The username contains illegal characters.\n";
		    } 
		    return error;
		}	
	  //validate password
	    function validatePassword(password) {
	        var error = "";
	        var illegalChars = /[\W_]/; // allow only letters and numbers 
	     
	        if (password == "") {
	            error = "You didn't enter a password.\n";
	        } else if ((password.length < 7) || (password.length > 15)) {
	            error = "The password length should be between 7 and 15! \n";
	        } else if (illegalChars.test(password)) {
	            error = "The password contains illegal characters.\n";
	        } else if (!(/[a-z]/.test(password) && /[A-Z]/.test(password) && /[0-9]/.test(password) )) {
	            error = "The password must contain at least one numeral, one upper and one lower case.\n";
	        } 
	       return error;
	    }  
	   //trim space
	    function trim(s)
	    {
	      return s.replace(/^\s+|\s+$/, '');
	    } 
       //validate email
	    function validateEmail(email) {
	        var error="";
	        var tfld = trim(email);                        // value of field with whitespace trimmed off
	        var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
	        var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
            if (tfld == "") {	            
	            error = "You didn't enter an email address.\n";
	        } else if (!emailFilter.test(tfld)) {              //test email for illegal characters
	            error = "Please enter a valid email address.\n";
	        } else if (tfld.match(illegalChars)) {
	            error = "The email address contains illegal characters.\n";
	        } 
	        return error;
	    }

  //If the server authentication fails, return warning message
	if(signinMessage) 
	    {   
		    
		    $form_modal.addClass('is-visible');	
		    login_selected();
		    if(signinMessage.match(/\bemail\b/i))
		    {
	    	$form_login.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible').text(signinMessage);
		    }
		    else
		    {
		    $form_login.find('input[type="password"]').toggleClass('has-error').nextAll('span').toggleClass('is-visible').text(signinMessage);	
		    }
	    }
    if(signupMessage)
    {
    	$form_modal.addClass('is-visible');	
    	signup_selected();
    	$form_signup.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible').text(signupMessage);
    }
    //Control login warning message
	$form_login.find('input[type="submit"]').on('click', function(event){
        var email = $form_login.find('input[type="email"]');
        var password = $form_login.find('input[type="password"]');
        $form_login.find('input[type="email"]').removeClass('has-error').next('span').removeClass('is-visible');
        $form_login.find('input[type="password"]').removeClass('has-error').nextAll('span').removeClass('is-visible');
        var error_email = validateEmail(email.val());
        if(error_email)
		{
        event.preventDefault();
		$form_login.find('input[type="email"]').toggleClass('has-error').next('span').addClass('is-visible').text(error_email);
        }
        else if(!password.val())
		{    
        event.preventDefault();
		$form_login.find('input[type="password"]').toggleClass('has-error').nextAll('span').addClass('is-visible').text("Please enter your password!");
        }
	});
	
	//Controll signup warning message
	$form_signup.find('input[type="submit"]').on('click', function(event){
		var email = $form_signup.find('input[type="email"]');
        var password = $form_signup.find('input[type="password"]');
        var name = $form_signup.find('input[name="name"]');
        var error_email = validateEmail(email.val());
        var error_password= validatePassword(password.val());
        var error_name = validateUsername(name.val());
        $form_signup.find('input[type="email"]').removeClass('has-error').next('span').removeClass('is-visible');
        $form_signup.find('input[type="password"]').removeClass('has-error').nextAll('span').removeClass('is-visible');
        $form_signup.find('input[name="name"]').removeClass('has-error').next('span').removeClass('is-visible');
        if(error_name)
		{
        event.preventDefault();
        $form_signup.find('input[name="name"]').toggleClass('has-error').next('span').addClass('is-visible').text(error_name);
        }     
        else if(error_email)
		{
        event.preventDefault();
        $form_signup.find('input[type="email"]').toggleClass('has-error').next('span').addClass('is-visible').text(error_email);
        }
        else if(error_password)
		{
        event.preventDefault();
        $form_signup.find('input[type="password"]').toggleClass('has-error').nextAll('span').addClass('is-visible').text(error_password);
        }
	});
	//Cotnrol password reset warning message
	$form_reset.find('input[type="submit"]').on('click', function(event){
		event.preventDefault();
	});
	//open modal
	$main_nav.on('click', function(event){
		   if ( $(event.target).is('.cd-signup') || $(event.target).is('.cd-signin'))
			{
		    //show modal layer
			$form_modal.addClass('is-visible');	
			//show the selected form
			( $(event.target).is('.cd-signup') ) ? signup_selected() : login_selected();
			}
 	});
	

	//close modal
	$('.cd-user-modal').on('click', function(event){
		if( $(event.target).is($form_modal) || $(event.target).is('.cd-close-form') ) {
			$form_modal.removeClass('is-visible');
		}	
	});
	//close modal when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$form_modal.removeClass('is-visible');
	    }
    });

	//switch from a tab to another
	$form_modal_tab.on('click', function(event) {
		event.preventDefault();
		( $(event.target).is( $tab_login ) ) ? login_selected() : signup_selected();
	});

	//hide or show password
	$('.hide-password').on('click', function(){
		var $this= $(this),
			$password_field = $this.prev('input');
		
		( 'password' == $password_field.attr('type') ) ? $password_field.attr('type', 'text') : $password_field.attr('type', 'password');
		( 'Hide' == $this.text() ) ? $this.text('Show') : $this.text('Hide');
		//focus and move cursor to the end of input field
		$password_field.putCursorAtEnd();
	});

	//show forgot-password form 
	$forgot_password_link.on('click', function(event){
		event.preventDefault();
		forgot_password_selected();
	});

	//back to login from the forgot-password form
	$back_to_login_link.on('click', function(event){
		event.preventDefault();
		login_selected();
	});
	

	function login_selected(){
		$form_login.addClass('is-selected');
		$form_signup.removeClass('is-selected');
		$form_forgot_password.removeClass('is-selected');
		$tab_login.addClass('selected');
		$tab_signup.removeClass('selected');
	}

	function signup_selected(){
		$form_login.removeClass('is-selected');
		$form_signup.addClass('is-selected');
		$form_forgot_password.removeClass('is-selected');
		$tab_login.removeClass('selected');
		$tab_signup.addClass('selected');
	}

	function forgot_password_selected(){
		$form_login.removeClass('is-selected');
		$form_signup.removeClass('is-selected');
		$form_forgot_password.addClass('is-selected');
	}
 




	//IE9 placeholder fallback
	//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
	if(!Modernizr.input.placeholder){
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
		  	}
		}).blur(function() {
		 	var input = $(this);
		  	if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.val(input.attr('placeholder'));
		  	}
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
		  	$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
			 		input.val('');
				}
		  	})
		});
	}

});


//credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
jQuery.fn.putCursorAtEnd = function() {
	return this.each(function() {
    	// If this function exists...
    	if (this.setSelectionRange) {
      		// ... then use it (Doesn't work in IE)
      		// Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
      		var len = $(this).val().length * 2;
      		this.setSelectionRange(len, len);
    	} else {
    		// ... otherwise replace the contents with itself
    		// (Doesn't work in Google Chrome)
      		$(this).val($(this).val());
    	}
	});
};