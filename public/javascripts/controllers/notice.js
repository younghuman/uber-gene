$(document).ready(function(){
	var notice_table = $('#notice-table');
	var $notice = io.connect(window.location.origin+'/notice');
	var insertFriendAdded = function(table, name, date){
		var div = $('<div>').append(
                $('<div>').html("[FriendAdded] <strong>"+name+"</strong> and you are friends now!")
                         .prop("class", "col-xs-7"),
                $('<div>').html("<span class='badge delete'>Delete</span>")
	                      .prop('class','col-xs-1'),   
                $('<div>').text(date).prop("class", "col-xs-4")
                         .css("color","#777777")
                       ).prop("class","friend-request row")
                       .css("background","#ffffff");
		table.append(div);
        return div;
	}
	var insertFriendRejected = function(table, name, date, passive){
		var string;
		if(!passive) string = "[FriendRejected] You have rejected friend request from <strong>"+name+"</strong> !";
		else string = "[FriendRejected] You have been rejected by <strong>"+name+"</strong> !";
		var div = $('<div>').append(
	               $('<div>').html(string)
	                        .prop("class", "col-xs-7"),
	               $('<div>').html("<span class='badge delete'>Delete</span>")
		                      .prop('class','col-xs-1'),   
	               $('<div>').text(date).prop("class", "col-xs-4")
	                        .css("color","#777777")
	                      ).prop("class","friend-request row")
	                      .css("background","#ffffff");
		table.append(div);
          return div;
	}
	$notice.emit('update notice');  
	$notice.emit('see notice');
	$notice.on('respond request', function(){
		$notice.emit('update notice');
	})
 //Insert notices
	$notice.on('update notice', function(notices)
	{ 
	  notice_table.children().remove();
      for (var i=0; i<notices.length; i++)
      {

        if(notices[i].type=="friendRequest")
        {	    	    
        	var name = notices[i].subjectName;
            var email= notices[i].subjectEmail;
            var id =   notices[i].subjectId;
            var type = notices[i].type;
            var message = notices[i].message;
            var status =  notices[i].status ;
            var date = notices[i].date;
	        if(status=="unseen")
	        {
         	  notice_table.append(
         	   $('<div>').append(
               $('<div>').html("[FriendRequest] <strong>"+name+"</strong>"+" wants to add you as a friend! <br>" +
               		"<span style='font-size:12px;color:#777777;padding:10px 20px;display:inline-block'>Message:"+message+"</span>")
                    .prop("class","col-xs-6"),
               $('<div>').html("<span class='badge reject'>Reject</span>")
                    .prop('class','col-xs-1'),
               $('<div>').html("<span class='badge accept'>Accept</span>")
                    .prop('class','col-xs-1'),
               $('<div>').prop("class", "col-xs-4").text(date)
                     .css("color","#777777")
             ).css("background","#ffffff")
             .prop('class','friend-request row')
             .attr('data-subject-email',email)
             .attr('data-subject-id',id)
             .attr('data-type', type)
             .attr('data-subject-name', name)
             )
	        }
	        else if(status=="accepted") 
	        insertFriendAdded(notice_table, name, date)
	        .attr('data-subject-email',email)
            .attr('data-subject-id',id)
            .attr('data-type', type)
            .attr('data-subject-name', name);
	        else if(status=="rejected") 
	        insertFriendRejected(notice_table, name, date)
	        .attr('data-subject-email',email)
            .attr('data-subject-id',id)
            .attr('data-type', type)
            .attr('data-subject-name', name);
        }
        else if(notices[i].type=="friendSend")
        {
        	var name = notices[i].ToName;
 	        var email= notices[i].ToEmail;
 	        var id =   notices[i].ToId
 	        var type = notices[i].type;
 	        var status =  notices[i].status;
 	        var date = notices[i].date;
 	        if(status=='unseen')
 	        {
 	        	notice_table.append(
 	        		$("<div>").append(
 	                   $('<div>').html("[FriendSend] You have sent <strong>"+name+"</strong>"+" a friend invitation ")
 	                        .prop("class","col-xs-7"),
 	                   $('<div>').html("<span class='badge recall'>Recall</span>")
 	                      .prop('class','col-xs-1'),
 	                   $('<div>').text(date).prop("class", "col-xs-4")
 	                            .css("color","#777777")
 	                 ).css("background","#ffffff")
 	                 .attr('data-to-email',email)
                     .attr('data-to-id',id)
                     .attr('data-type', type)
                     .attr('data-to-name', name)
 	                 .prop('class','friend-send row')
 	               )
            }
 	        else if(status=="accepted") 
 	        insertFriendAdded(notice_table, name, date)
 	                 .attr('data-to-email',email)
                     .attr('data-to-id',id)
                     .attr('data-type', type)
                     .attr('data-to-name', name)
 	                 .prop('class','friend-send row');
 	       else if(status=="rejected") 
 	       insertFriendRejected(notice_table, name, date, true)
 	 	                 .attr('data-to-email',email)
 	                     .attr('data-to-id',id)
 	                     .attr('data-type', type)
 	                     .attr('data-to-name', name)
 	 	                 .prop('class','friend-send row');
        }
        else if(notices[i].type=="systemLogs"){
            var msg  = notices[i].message;
 	        var id =   notices[i].Id;
 	        var type = notices[i].type;
 	        var date = notices[i].date;
 	        notice_table.append(
	        		$("<div>").append(
	                   $('<div>').html("[SystemLogs] " + msg)
	                       .prop("class","col-xs-7"),
	                   $('<div>').html("<span class='badge delete'>Delete</span>")
	                      .prop('class','col-xs-1'),
	                   $('<div>').text(date).prop("class", "col-xs-4")
	                            .css("color","#777777")
	                 ).css("background","#ffffff")
                    .attr('data-id',id)
                    .attr('data-type', type)
	                .prop('class','system-logs row')
	               )
        	
        }
      } //Insert notices loop end
        notice_table.find('div.row').css('padding','10px 5px');
        notice_table.find('span.badge').css('cursor', 'pointer');
       //click badges
        notice_table.find('.friend-request span.badge').click(function(){
        	var parent = $(this).parents(".friend-request");
        	var message = {email:parent.data('subject-email'),
        			       id:parent.data('subject-id'),
        			       type:parent.data('type'),
        			       name:parent.data('subject-name')}
        	if($(this).hasClass('accept')) message.response = "accept";
        	else if($(this).hasClass('reject')) message.response = "reject";
        	else if($(this).hasClass('delete')) message.response = "delete";
        	$notice.emit('respond request', message);                         
        	}); 
        notice_table.find('.friend-send span.badge').click(function(){
        	var parent = $(this).parents(".friend-send");
        	var message = {email:parent.data('to-email'),
 			                 id:parent.data('to-id'),
 			               type:parent.data('type'),
 			               name:parent.data('to-name')}
        	if($(this).hasClass('recall')) message.response = "recall";
        	else if($(this).hasClass('delete')) message.response = "delete";
        	$notice.emit('respond request', message);
        });
        notice_table.find('.system-logs span.badge').click(function(){
        	var parent = $(this).parents(".system-logs");
        	var message = {  id:parent.data('id'),
 			               type:parent.data('type')}
        	if($(this).hasClass('delete')) message.response = "delete";
        	$notice.emit('respond request', message);
        });
        
      });
});