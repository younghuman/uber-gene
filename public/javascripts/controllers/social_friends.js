$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip()
    $('li.list-group-item.focus').removeClass('focus');
    $('li.list-group-item#friends').addClass('focus');
    var add_friends_target;
    var delete_friends_target;
    var chat = io.connect(window.location.origin+'/friend');
    var closeAddFriends = function() {
  	   $('#add-friend').removeClass('is-visible');
    }
    var closeDeleteFriends = function() {
   	   $('#delete-friend').removeClass('is-visible');
     }
    var openAddFriends = function(e)  {
       add_friends_target = {
    		       id:$(e.target).attr('data-id'),
    		       email:$(e.target).attr('data')
       }                                       
 	   $('#add-friend').addClass('is-visible');
    }
    var openDeleteFriends = function(e)  {
        delete_friends_target = {
     		       id:$(e.target).attr('data-id'),
     		       email:$(e.target).attr('data')
        }                                       
  	   $('#delete-friend').addClass('is-visible');
     }
    var searchFriends = function(event) {
    	event.preventDefault();
    	var name = $('input#search').val();
    	chat.emit('search friend', name);
    	$('input#search').val('');
    }
    var addFriends = function() {
    	var msg = $('textarea.add-friend').val();
    	if(add_friends_target)
       {
    	chat.emit('add friend', {email:add_friends_target.email, id:add_friends_target.id, msg:msg  });
        }
    	closeAddFriends();
    }
    var deleteFriends = function() {
    	if(delete_friends_target)
       {
    	chat.emit('delete friend', {email:delete_friends_target.email, id:delete_friends_target.id });
        }
    	closeDeleteFriends();
    }
    var addServerMessage = function(element, msg) {
    	 $(element).prepend(
          		  $('<div>').prop('class','col-xs-6 msg')
          		  .append($('<div class="alert alert-'+msg[1]+'" role="alert">')
                  .css({"font-weight":"700","padding":"8px","clear":"both"})
          	      .text(msg[0])  	)		
          	);
      }
//Receive the log from the server for adding friends
    chat.on('delete friend', function(msg){
    	$('div.msg').remove();
    	if(msg){
    		addServerMessage($('.row#top'),msg);
    		$('span.delete-friends[data="'+delete_friends_target.email+'"]').parent().parent().remove();
    	}
    })
    
    chat.on('add friend', function(msg){
       $('div.msg').remove();
       if(msg) {
    	   addServerMessage($('.row#top'),msg);
       }
       else {
    	   $('.row#top').prepend(
         		  $('<div>').attr('class','col-xs-6 msg')
         		  .append($('<div class="alert alert-danger" role="alert" style="font-weight:700;padding:7px 10px;margin:0 ">')
         	      .text('[Error]Add friend request failed!') ) 			
         	)
       }
    });
    
//Add data after the friends searched for have been returned
//This part is an example of how to use pure Javascript to create a HTML element
    chat.on('search friend', function(users){
    	$('div.found-friends').remove();
    	$('div.msg').remove();
        if(users.length){
      //alert message
        	$('.row#top').prepend(
          		  $('<div>').prop('class','col-xs-6 msg')
          		  .append($('<div class="alert alert-info" role="alert">')
                  .css({"font-weight":"700","padding":"8px","clear":"both"})
          	      .text('Found')  	)		
          	);
        	$(".row#top").after(
        	  $('<div>').attr('class','row found-friends')	
        	         .append(
        	        		 $('<div>').attr('class',"col-xs-6").append(
        	        	    $('<ul>').attr('class','list-group found-friends')
        	                 ))
        	);
            for (var i=0;i<users.length;i++){
            	$("ul.found-friends").append(
            			 $('<li>').prop('class','list-group-item col-xs-12')
            			          .css('padding', '10px 20px 10px 0')
                                  .append($('<span class="col-xs-8">')             			          
            			        		.text(users[i].name),
                                       //The email icon
                                       	$('<span class="glyphicon glyphicon-envelope col-xs-1" aria-hidden="true"></span>')
             			        	   .attr("data-toggle","tooltip")
        			        		   .attr("data-placement","left")
        			        		   .css({
        			        			  "font-size":"20px","color":"#444466","float":"right"})
        			        	       .attr('title',users[i].email) 
        			        	       .tooltip(),
        			        	       //The add friends icon
        			        	  		$('<span class="glyphicon glyphicon-plus add-friends col-xs-1" aria-hidden="true">')
            			        		  .css({"font-size":"20px","color":"#55aaef", "float":"right"})
          			        		  .attr('data-id',users[i].id)
          			        		  .attr('data',users[i].email)
          			        		  .click(openAddFriends)
                               	  
            			 ) ) }
            
           }
        else{
        	$('.row#top').before(
        		  $('<div>').attr('class','col-xs-6 msg')
        		  .append($('<div class="alert alert-danger" role="alert" style="font-weight:700;padding:7px 10px;margin:0 ">').text('Not found') ) 			
        	)
         }
    });
   //Trigger search friends
    $('#search-button').click(
    		searchFriends		
    );
    $('input#search').bind('keypress', function(e){
    	if(e.keyCode==13){
    		 searchFriends(e);
         } 
      });
   //Close the form
    $('.cd-user-modal').on('click', function(event){
		if( $(event.target).is($('#add-friend'))  ){
			closeAddFriends();
		}	
	});
	//close modal when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		closeAddFriends();
	    }
    });
   $('#add-friend button.close-add-friends').click(closeAddFriends);
   $('#add-friend button.send-add-friends').click(addFriends);
   $('#delete-friend button.no-delete-friends').click(closeDeleteFriends);
   $('#delete-friend button.yes-delete-friends').click(deleteFriends);
   $('span.add-friends').click(openAddFriends);  
   $('span.delete-friends').click(openDeleteFriends);


});


