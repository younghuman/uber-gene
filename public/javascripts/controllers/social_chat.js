$(document).ready(function(){

     var username = "#{user.data.name}";
	 var chat = io.connect(window.location.origin+'/chat');
	 $('li.list-group-item.focus').removeClass('focus');
	 $('li.list-group-item#chat').addClass('focus');
     chat.on('chat message', function(msg){
    	 $('ul#messages').append($('<li>').html("<span>"+msg.username+"</span>"+":  "+msg.msg)); 
    	 $('ul#messages').html($('ul#messages').html().replace(/\n/g,'<br/>'));
    	 $('ul#messages span').css('color',msg.color);
    	 $('ul#messages').animate({scrollTop:Number.POSITIVE_INFINITY});
     });
	 
     //Send chat message
     $('button#submit').click(function() {
    	 event.preventDefault();
         chat.emit('chat message', $("textarea#chat").val());
    	 $("textarea#chat").val('');
     });
     $('textarea#chat').bind('keypress', function(e){
    	if(e.keyCode==13){
    		 e.preventDefault();
    		 chat.emit('chat message', $("textarea#chat").val());
    		 $("textarea#chat").val('');
         } 
      });
  });